var Session = require('../models/Session'),
    User = require('../models/User'),
    Boom = require('boom'),
    flow = require('flow.js').flow;


module.exports = function (server, options) {

    return {
        authenticate: function (request, reply) {
            flow()
                .seq("session", function (cb) {
                    Session.findOne({
                        _id: request.headers["session-token"]
                    }, function (err, session) {
                        if (!err && !session) {
                            err = Boom.forbidden("No session found");
                        }
                        cb(err, session);
                    });
                })
                .seq("user", function (cb) {
                    User.findOne({
                        _id: this.vars.session.user,
                        state: User.STATE.REGISTERED
                    }, function (err, user) {
                        if (!err && !user) {
                            err = Boom.forbidden("No user found for session");
                        }
                        cb(err, user);
                    })
                })
                .exec(function (err, results) {
                    if (err) {
                        reply(err);
                    } else {
                        reply.continue({
                            credentials: results.user,
                            artifacts: {
                                sessionId: results.session.id,
                                auth: results.session.auth
                            }
                        });


                    }
                });

        }
    }

};