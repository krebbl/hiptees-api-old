var flow = require('flow.js').flow,
    Product = require('../models/Product'),
    ProductType = require('../models/ProductType'),
    fs = require("fs"),
    images = require("./images"),
    request = require("request"),
    crypto = require('crypto');

var apiRequest = request.defaults({
    gzip: true,
    json: true,
    headers: {
        'content-type': 'application/json; charset=UTF-8'
    }
});

function SHA1(key) {
    var shasum = crypto.createHash('sha1');
    shasum.update(key);
    return shasum.digest('base64');
}

function signature(method, url, secret, time) {
    var data = method + " " + url + " " + time + " " + secret;
    return SHA1(data);
}


function createSprdDesign(config, printout, callback) {

    flow()
        // create design
        .seq("design", function (cb) {
            var url = config.apiEndpoint + "/shops/" + config.shop + "/designs";

            console.log(url);
            apiRequest.post(url, {
                qs: createQueryParameter("post", url, config.apiKey, config.secret),
                body: {
                    description: "",
                    designServiceState: null,
                    name: printout,
                    restrictions: {}
                }
            }, function (err, response, body) {
                if (response.statusCode == 201) {
                    cb(null, body);
                } else {
                    console.log(response);
                    cb("design failed : " + response.statusMessage);
                }
            });
        })
        // upload shit
        .seq(function (cb) {
            console.log(this.vars.design.id);
            var stat = fs.statSync(__dirname + '/../' + printout);
            console.log(stat.size);
            fs.createReadStream(__dirname + '/../' + printout).pipe(request.put(config.imageEndpoint + "/designs/" + this.vars.design.id, {
                qs: {
                    method: "put",
                    mediaType: "png",
                    apiKey: config.apiKey
                },
                headers: {
                    'Content-Length': stat.size
                }
            }, function (err, response, body) {
                if (response.statusCode == 200) {
                    console.log("uploaded image");
                    cb(null);
                } else {
                    console.log(body);
                    cb("image upload failed : " + response.statusMessage);
                }
            }))
        })
        .exec(function (err, results) {
            callback && callback(err, results.design);
        });

}

function createQueryParameter(method, url, apiKey, secret) {
    var time = new Date().getTime();

    return {
        mediaType: "json",
        sig: signature(method, url, secret, time),
        time: time,
        method: method,
        apiKey: apiKey
    };
}


module.exports = {

    createProduct: {
        handler: function (req, reply) {
            var id = req.params.id,
                config = req.server.settings.app.config.spreadshirt;

            flow()
                .seq("product", function (cb) {
                    Product.findOne({_id: id}).lean().exec(cb);
                })
                .seq("productType", function (cb) {
                    ProductType.findOne({_id: this.vars.product.productType.id}).lean().exec(cb);
                })
                .seq("sprdProductType", function (cb) {
                    apiRequest.get(config.apiEndpoint + "/shops/" + config.shop + "/productTypes/" + this.vars.productType.sprdId, {
                        qs: {
                            mediaType: "json"
                        }
                    }, function (err, response, body) {
                        if (!err && response.statusCode == 200) {
                            cb(err, body);
                        } else {
                            cb(err || "PRODUCT_TYPE_NOT_FOUND");
                        }
                    });
                })
                .seq("printout", function (cb) {
                    images.renderPrintout(this.vars.product._id, cb);
                })
                .seq("sprdDesign", function (cb) {
                    createSprdDesign(config, this.vars.printout, cb);
                })
                .seq("sprdConfig", function () {
                    return {
                        offset: {
                            x: 0,
                            y: 30  // TODO: correct offset
                        },
                        content: {
                            dpi: "25.4",
                            svg: {
                                image: {
                                    designId: this.vars.sprdDesign.id,
                                    height: 250,
                                    printColorRGBs: "",
                                    transform: "",
                                    width: 308 // TODO: printAreaWidth
                                }
                            }
                        },
                        printArea: {
                            id: 4 // TODO: printArea id
                        },
                        printType: {
                            id: 17
                        },
                        type: "design"
                    };

                })
                .seq("sprdProduct", function () {
                    return {
                        appearance: {
                            id: this.vars.product.appearance.id
                        },
                        defaultValues: {
                            // TODO
                            defaultView: {
                                id: 1
                            }
                        },
                        productType: {
                            id: this.vars.sprdProductType.id
                        },
                        configurations: [this.vars.sprdConfig],
                        restrictions: {
                            customizable: true,
                            example: false,
                            freeColorSelection: true,
                            softBoundarySupported: true
                        }
                    }
                })
                .seq("body", function (cb) {
                    var url = config.apiEndpoint + "/shops/" + config.shop + "/products";
                    apiRequest.post(url, {
                        qs: createQueryParameter("post", url, config.apiKey, config.secret),
                        body: this.vars.sprdProduct
                    }, function (err, response, body) {
                        if (response.statusCode == 201) {
                            cb(null, body);
                        } else {
                            console.log(body);
                            cb("product failed : " + response.statusMessage);
                        }
                    });
                })
                .exec(function (err, results) {
                    if (err) {
                        console.log(err);
                        reply(err);
                    } else {
                        console.log("success");
                        console.log(results.body);
                        reply(results.body);
                    }

                });
        }
    }
};