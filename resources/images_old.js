var phantom = require('phantom'),
    flow = require('flow.js').flow,
    fs = require('fs'),
    im = require('imagemagick');

var currentProductRenderings = {};
var sizes = [300, 600, 900];
var config = {};

var renderProductImages = function (id, cb) {

    var currentProductRendering = currentProductRenderings[id];
    if (currentProductRendering && cb) {
        currentProductRendering.callbacks.push(cb);
    } else {

        if (currentProductRendering && currentProductRendering.flow) {
            currentProductRendering.flow.cancelled = true;
        }

        sizes.forEach(function (size) {
            try {
                fs.unlinkSync(productImagePath(id, size));
            } catch (e) {

            }
        });
        var f = flow()
            .seq("base64", function (cb) {
                if (!f.cancelled) {
                    renderProduct(id, cb);
                } else {
                    this.end();
                }
            })
            .seq("buf", function () {
                return new Buffer(this.vars.base64, 'base64');
            })
            .seqEach(sizes, function (size, cb) {
                if (!f.cancelled) {
                    im.resize({
                        srcData: this.vars.buf,
                        dstPath: productImagePath(id, size),
                        width: size
                    }, function (err, stdIn, stdOut) {
                        cb(err);
                    });
                } else {
                    this.end();
                }
            });

        currentProductRenderings[id] = currentProductRendering || {
            callbacks: []
        };

        currentProductRenderings[id].flow = f;

        if (cb) {
            currentProductRenderings[id].callbacks.push(cb);
        }

        f.exec(function (err) {
            if (!f.cancelled) {
                var callbacks = currentProductRenderings[id].callbacks;
                currentProductRenderings[id] = null;
                for (var i = 0; i < callbacks.length; i++) {
                    var callback = callbacks[i];
                    callback(err);
                }
            } else {
                console.log("CANCELLED!");
            }
        });
    }
};

var openPhantoms = {};

process.on('exit', function () {
    for (var processId in openPhantoms) {
        if (openPhantoms.hasOwnProperty(processId)) {
            openPhantoms[processId].exit();
        }

    }
});

var renderProduct = function (id, cb) {

    phantom.create(function (ph) {
        openPhantoms[ph.process.pid] = ph;

        ph.onExit = function () {

            delete openPhantoms[ph.process.pid];
        };

        ph.createPage(function (page) {
            page.setViewportSize(1600, 1600, function () {
                page.open(config.baseUri + "/renderer.html#/product/" + id, function (status) {
                    if (status !== "success") {
                        exited = true;
                        ph.exit();
                        cb && cb("renderer not reachable");
                    }
                });
                var exited = false;

                page.set('onConsoleMessage', function (msg, lineNum, sourceId) {
                    console.log('CONSOLE: ' + msg + ' (from line #' + lineNum + ' in "' + sourceId + '")');
                });

                page.set('onAlert', function (msg) {
                    if (msg == "loaded") {
                        exited = true;
                        page.renderBase64('PNG', function (base64) {
                            exited = true;
                            ph.exit();
                            cb && cb(null, base64);

                        });
                    } else {
                        exited = true;
                        ph.exit();
                        cb("ERROR");
                    }
                });

                setTimeout(function () {
                    if (!exited) {
                        ph.exit();
                        cb && cb("timeout reached", null);
                    }
                }, 5000);

            });
        });
    });
};

var renderPrintout = function (id, cb) {
    phantom.create(function (ph) {
        openPhantoms[ph.process.pid] = ph;

        ph.onExit(function () {
            delete openPhantoms[ph.process.pid];
        });

        ph.createPage(function (page) {
            var exited = false;

            page.set('onConsoleMessage', function (msg, lineNum, sourceId) {
                console.log('CONSOLE: ' + msg + ' (from line #' + lineNum + ' in "' + sourceId + '")');
            });

            page.set('onAlert', function (msg) {
                if (msg == "loaded") {
                    var filePath = printoutPath(id);
                    page.render(filePath, {format: 'png'});
                    ph.exit();
                    cb && cb(null, filePath);
                }
            });

            page.open(config.baseUri + "/renderer.html#/printout/" + id, function (status) {

            });

            setTimeout(function () {
                if (!exited) {
                    ph.exit();
                    cb && cb("RENDER_TIMEOUT_REACHED", null);
                }
            }, 30000);
        });
    });
};


function productImagePath(id, size) {
    return process.cwd() + "/generated/images/products/" + id + "-" + size + ".png";
}

function printoutPath(id) {
    return process.cwd() + "/generated/printout_" + id + ".png";
}

module.exports = {
    config: function (baseUri) {
        config.baseUri = baseUri;
    },
    renderPrintout: renderPrintout,
    printouts: {
        handler: function (request, reply) {
            var filePath = printoutPath(request.params.id);
            fs.exists(filePath, function (exists) {
                if (exists) {
                    reply.file(filePath);
                } else {
                    renderPrintout(request.params.id, function (err, filePath) {
                        if (!err) {
                            reply.file(filePath);
                        }
                    })
                }
            });

        }
    },
    renderProductImages: renderProductImages,
    products: {
        auth: false,
        handler: function (request, reply) {
            var id = request.params.id,
                size = parseInt(request.params.size);

            if (sizes.indexOf(size) == -1) {
                reply(Boom.badData("Size now allowed"));
                return;
            }

            var filePath = productImagePath(id, size);

            fs.exists(filePath, function (exists) {
                if (exists) {
                    reply.file(filePath);
                } else {
                    renderProductImages(id, function () {
                        reply.file(filePath);
                    })
                }
            });
        }

    }


};