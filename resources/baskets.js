var Joi = require('joi'),
    Boom = require('boom'),
    Basket = require('../models/Basket'),
    Product = require('../models/Product'),
    ProductType = require('../models/ProductType'),
    flow = require('flow.js').flow,
    ImageRenderer = require('../util/ImageRenderer'),
    SprdConverter = require('../util/SprdConverter'),
    ProductResource = require('./products'),
    ProductTypeResource = require('./productTypes'),
    _ = require('lodash');


var calcItemPrice = function (item) {
    // TODO: implement
    return 19.99;
};

var basketToJson = function (basket) {
    var itemPrice,
        totalPrice = 0,
        totalQuantity = 0,
        jsonBasket = {
            id: basket._id,
            items: []
        };

    var items = [],
        jsonItem;

    basket.items.forEach(function (item) {
        itemPrice = calcItemPrice(item);
        totalPrice += itemPrice * item.quantity;
        totalQuantity += item.quantity;
        jsonItem = {
            id: item._id,
            product: {
                id: item.product
            },
            quantity: item.quantity,
            price: itemPrice,
            size: {
                id: item.size.id
            }
        };
        jsonBasket.items.push(jsonItem);
    });

    jsonBasket.totalQuantity = totalQuantity;
    jsonBasket.totalPrice = totalPrice.toFixed(2);

    return jsonBasket;
};

var buildCombinedBasket = function (request, basket, cb) {
    var ret = {
        basket: basketToJson(basket),
        products: null,
        productTypes: null
    };

    var productIds = _.map(basket.items, function (item) {
        return item.product;
    });

    flow()
        .seq("products", function (cb) {
            Product.find({
                _id: {
                    $in: _.map(productIds, function (id) {
                        return id.toString()
                    })
                }
            }).select('id name creator productType appearance __v').exec(cb);
        })// TODO add request
        .seq("productTypes", function (cb) {
            ProductType.find({
                _id: {
                    $in: _.map(this.vars.products, function (p) {
                        return p.productType.toString();
                    })
                }
            }).lean().exec(cb);
        })
        .seq(function () {
            ret.products = _.map(this.vars.products, ProductResource.helper.toResource(request));
            ret.productTypes = _.map(this.vars.productTypes, ProductTypeResource.helper.toResource(request));
        })
        .exec(function (err) {
            cb(err, ret);
        });

};

var checkProductAndSize = function (productId, sizeId, cb) {
    flow().
        seq("product", function (cb) {
            Product.findOne({
                _id: productId
            }).populate('productType').exec(function (err, product) {
                if (!product) {
                    err = Boom.badRequest("product not found");
                }
                cb(err, product);
            });
        })
        .seq(function () {
            var sizes = this.vars.product.productType.sizes,
                sizeFound = false;
            for (var i = 0; i < sizes.length; i++) {
                var s = sizes[i];
                if (s.id == sizeId) {
                    sizeFound = true;
                    return s;
                }
            }

            throw Boom.badRequest("Size not allowed for product", {size: sizeId});
        })
        .exec(cb);
};

var findBasket = function (basketId, cb) {
    Basket.findOne({
        _id: basketId
    }, function (err, basket) {
        if (!basket) {
            err = Boom.badRequest("basket not found");
        }
        cb(err, basket);
    });
};

var AddPayloadSchema = {
    "basket": Joi.object({
        id: Joi.string()
    }),
    "product": Joi.object({
        id: Joi.string().required()
    }).required(),
    "size": Joi.object({
        id: Joi.string().required()
    }).required(),
    "quantity": Joi.number().required()
};

var ItemPayloadSchema = {
    "basket": Joi.object({
        id: Joi.string().required()
    }),
    "item": Joi.object({
        id: Joi.string().required()
    }).required(),
    "size": Joi.object({
        id: Joi.string().required()
    }),
    "quantity": Joi.number()
};

var RemoveItemPayloadSchema = {
    "basket": Joi.object({
        id: Joi.string().required()
    }),
    "item": Joi.object({
        id: Joi.string().required()
    }).required()
};

module.exports = {
    combinedBasket: {
        auth: false,
        handler: function (request, reply) {
            flow()
                .seq("basket", function (cb) {
                    Basket.findOne({
                        _id: request.params.id
                    }, function (err, basket) {
                        if (!basket) {
                            err = Boom.notFound("basket not found");
                        }

                        cb(err, basket);
                    })
                })
                .seq("combinedBasket", function (cb) {
                    buildCombinedBasket(request, this.vars.basket, cb);
                })
                .exec(function (err, results) {
                    if (err) {
                        reply(err);
                    } else {
                        reply(results.combinedBasket).code(200);
                    }
                });
        }
    },
    getOne: {
        auth: false,
        handler: function (request, reply) {
            Basket.findOne({
                _id: request.params.id
            }, function (err, basket) {
                if (!basket) {
                    err = Boom.notFound("basket not found");
                }
                if (!err) {
                    reply(basketToJson(basket)).code(200);
                } else {
                    reply(err);
                }
            });
        }
    },

    addItem: {
        auth: false,
        validate: {
            payload: AddPayloadSchema
        },
        handler: function (request, reply) {
            var payload = request.payload;

            var basketId = payload.basket ? payload.basket.id : null,
                productId = payload.product.id,
                quantity = payload.quantity,
                sizeId = payload.size.id;

            flow()
                .seq("basket", function (cb) {
                    if (basketId) {
                        Basket.findOne({
                            _id: basketId
                        }, function (err, basket) {
                            if (!basket) {
                                basket = new Basket();
                            }
                            cb(null, basket);
                        });
                    } else {
                        var basket = new Basket();
                        cb(null, basket);
                    }
                })
                .seq(function (cb) {
                    checkProductAndSize(productId, sizeId, cb);
                })
                .seq("savedBasket", function (cb) {
                    var basketItems = this.vars.basket.items;
                    var basketItem = null;

                    for (var i = 0; i < basketItems.length; i++) {
                        var item = basketItems[i];
                        if (item.product.toString() == productId && item.size.id == sizeId) {
                            basketItem = item;
                            break;
                        }
                    }

                    if (basketItem) {
                        basketItem.quantity += quantity;
                    } else {
                        basketItems.push({
                            product: productId,
                            quantity: quantity,
                            "size.id": sizeId
                        });

                        setTimeout(function(){
                            ImageRenderer.checkAndRenderPrintout(productId);
                        },0);
                    }


                    this.vars.basket.save(cb);
                })
                .seq("payload", function (cb) {
                    buildCombinedBasket(request, this.vars.savedBasket, cb);
                })
                .exec(function (err, results) {
                    if (!err) {
                        reply(results.payload).code(200);
                    } else {
                        reply(err);
                    }
                });


            // returns basket payload
        }
    },

    removeItem: {
        auth: false,
        validate: {
            payload: RemoveItemPayloadSchema
        },
        handler: function (request, reply) {
            var itemId = request.payload.item.id,
                basketId = request.payload.basket.id;

            flow()
                .seq("basket", function (cb) {
                    findBasket(basketId, cb);
                })
                .seq("savedBasket", function (cb) {
                    this.vars.basket.items = _.filter(this.vars.basket.items, function (item) {
                        return item.id != itemId;
                    });
                    this.vars.basket.save({safe: true}, cb);
                })
                .seq("payload", function (cb) {
                    buildCombinedBasket(request, this.vars.savedBasket, cb);
                })
                .exec(function (err, results) {
                    if (!err) {
                        reply(results.payload).code(200);
                    } else {
                        reply(err);
                    }
                });
        }
    },

    updateItem: {
        validate: {
            payload: ItemPayloadSchema
        },
        auth: false,
        handler: function (request, reply) {
            var payload = request.payload;
            var basketId = payload.basket.id,
                itemId = payload.item.id,
                size = payload.size,
                quantity = payload.quantity;


            flow()
                .seq("basket", function (cb) {
                    findBasket(basketId, cb);
                })
                .seq("item", function () {
                    var item = _.find(this.vars.basket.items, function (item) {
                        return item._id == itemId;
                    });

                    if (!item) {
                        throw Boom.badImplementation("Wrong item id");
                    }
                    return item;
                })
                .seq(function (cb) {
                    if (size) {
                        checkProductAndSize(this.vars.item.product, size.id, cb);
                    } else {
                        cb();
                    }
                })
                .seq(function () {

                    if (quantity > 0) {
                        this.vars.item.quantity = quantity;
                    }

                    if (size) {
                        var updatedItem = this.vars.item;
                        updatedItem.size.id = size.id;

                        var anotherItem = _.find(this.vars.basket.items, function (item) {
                            return updatedItem.id !== item.id &&
                                updatedItem.product.toString() == item.product.toString() &&
                                updatedItem.size.id == item.size.id;
                        });

                        if (anotherItem) {
                            anotherItem.quantity += updatedItem.quantity;
                            this.vars.basket.items = _.filter(this.vars.basket.items, function (item) {
                                return item.id != updatedItem.id;
                            });
                        }
                    }
                })
                .seq("savedBasket", function (cb) {
                    this.vars.basket.save(cb)
                })
                .seq("payload", function (cb) {
                    buildCombinedBasket(request, this.vars.savedBasket, cb);
                })
                .exec(function (err, results) {
                    if (!err) {
                        reply(results.payload).code(200);
                    } else {
                        reply(err);
                    }
                });

        }
    },
    checkout: {
        auth: false,
        handler: function (request, reply) {
            SprdConverter.convertBasket(request.payload.basket.id, function (err, basket) {
                if (err) {
                    reply(err)
                } else {
                    reply(basket);
                }
            })


        }
    }

};