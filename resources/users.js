var Joi = require('joi'),
    Boom = require('boom'),
    flow = require('flow.js').flow,
    User = require('../models/User'),
    Product = require('../models/Product'),
    resolveHost = require('../helpers/resolveHost');

module.exports = {
    getAll: {
        handler: function (request, reply) {
            User.find({}, function (err, users) {
                if (!err) {
                    reply(users);
                } else {
                    reply(Boom.badImplementation(err)); // 500 error
                }
            });
        }
    },

    getOne: {
        auth: false,
        handler: function (request, reply) {
            flow()
                .seq("user", function (cb) {
                    User.findOne({
                        username: request.params.username,
                        state: User.STATE.REGISTERED
                    }, "id name username email registrationDate profileImage").exec(function (err, user) {
                        if (!err && !user) {
                            err = Boom.notFound("user not found");
                        }

                        cb(err, user);
                    })
                })
                .seq("countPublished", function (cb) {
                    Product.count({
                        "creator": this.vars.user.id,
                        "state": Product.STATE.PUBLIC
                    }, cb);
                })
                .exec(function (err, results) {
                    if (!err) {
                        var user = results.user.toJSON();
                        user.countPublished = results.countPublished;
                        var host = resolveHost(request);
                        var baseUri = host + request.path;
//                        user.drafts = {
//                            href: uri + "/users/me/drafts"
//                        };

                        user.published = {
                            href: baseUri + "/published"
                        };

                        reply(user);
                    } else {
                        reply(err);
                    }
                });
        }
    },

    me: {
        handler: function (request, reply) {
            flow()
                .seq("user", function (cb) {
                    User.findOne({
                        _id: request.auth.credentials._id
                    }, "id name username email roles registrationDate profileImage").exec(cb)
                })
                .seq("countPublished", function (cb) {
                    Product.count({
                        "creator": this.vars.user.id,
                        "state": Product.STATE.PUBLIC
                    }, cb);
                })
                .seq("countDrafts", function (cb) {
                    Product.count({
                        "creator": this.vars.user.id,
                        "state": Product.STATE.DRAFT
                    }, cb);
                })
                .seq("countPrivates", function (cb) {
                    Product.count({
                        "creator": this.vars.user.id,
                        "state": Product.STATE.PRIVATE
                    }, cb);
                })
                .exec(function (err, results) {
                    if (!err) {
                        var user = results.user.toJSON();
                        user.id = "me";
                        user.countPublished = results.countPublished;
                        user.countDrafts = results.countDrafts;
                        user.countPrivates = results.countPrivates;
                        var uri = request.server.info.uri;

//                        user.drafts = {
//                            href: uri + "/users/me/drafts"
//                        };

//                        user.published = {
//                            href: uri + "/users/me/published"
//                        };

                        reply(user);
                    } else {
                        reply(Boom.badImplementation(err));
                    }
                });
        }
    }

};