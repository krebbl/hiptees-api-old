var Joi = require('joi'),
    Boom = require('boom'),
    FB = require('fb'),
    Twitter = require('twitter'),
    flow = require('flow.js').flow,
    User = require('../models/User'),
    Session = require('../models/Session'),
    ReservedUsernames = require('../util/ReservedUsernames');


var authenticateForTest = function (payload, cb) {
    var email = payload.email;
    flow()
        .seq("user", function (cb) {
            User.findOne({
                'email': email
            }, function (err, user) {
                cb(null, user);
            });
        })
        .seq("user", function () {
            var user = this.vars.user;
            if (!user) {
                user = new User({
                    email: email,
                    name: email.split("@").shift(),
                    state: User.STATE.NEW
                });
            }
            user.roles = ["customer"];
            user.state = user.state || User.STATE.NEW;

            return user;
        })
        .seq(function (cb) {
            if (this.vars.user.state == User.STATE.NEW) {
                setUniqueUsername(this.vars.user, cb);
            } else {
                cb();
            }
        })
        .seq("user", function (cb) {
            this.vars.user.save(cb);
        })
        .exec(function (err, results) {
            if (err) {
                console.log(err);
            }
            cb && cb(err, results.user);
        });
};

var isUsernameAvailable = function (username, cb) {
    if (ReservedUsernames.indexOf(username) > -1) {
        cb(Boom.badRequest("username not allowed"))
    } else if (/^[a-z0-9_-]{3,15}$/.test(username)) {
        cb(Boom.badRequest("username not allowed"));
    } else {
        User.findOne({
            state: User.STATE.REGISTERED,
            username: username
        }, function (err, user) {
            cb(err, !user);
        });
    }
};

var setUniqueUsername = function (user, cb) {
    var username;
    if (user.email) {
        user.email.split("@").shift()
    } else if (user.username) {
        username = user.username;
    }
    flow()
        .seq("available", function (cb) {
            isUsernameAvailable(username, cb);
        })
        .exec(function (err, results) {
            if (!err) {
                if (results.available) {
                    user.username = username;
                } else {
                    user.username = username + Math.round((Math.random() * 5000));
                }
            }
            cb(err);
        })
};

var authenticateWithFB = function (accessToken, cb) {
    flow()
        .seq("fbUser", function (cb) {
            FB.api('/me', {
                fields: ['id', 'name', 'email', 'picture.width(150).height(150)'],
                access_token: accessToken
            }, function (res) {
                console.log(res);
                if (!res || res.error) {
                    cb(!res ? 'FB_ERROR_OCCURED' : res.error);
                } else {
                    cb(null, res)
                }
            });

        })
        .seq("user", function (cb) {
            User.findOne({'email': this.vars.fbUser.email}, function (err, user) {
                console.log(user);
                cb(null, user);
            })
        })
        .seq("user", function () {
            var fbUser = this.vars.fbUser;
            var fbOauth = {
                id: fbUser.id
            };
            var user = this.vars.user;
            if (!user) {
                user = new User({
                    email: fbUser.email,
                    name: fbUser.name,
                    state: User.STATE.NEW
                });
            }
            var oauth = user.oauth || {};
            if (!oauth.facebook) {
                oauth.facebook = fbOauth;
            }
            user.roles = ["customer"];
            user.oauth = oauth;
            user.profileImage = fbUser.picture.data.url;
            return user;
        })
        .seq(function (cb) {
            if (this.vars.user.state == User.STATE.NEW) {
                setUniqueUsername(this.vars.user, cb);
            } else {
                cb();
            }
        })
        .seq("user", function (cb) {
            this.vars.user.save(cb);
        })
        .exec(function (err, results) {
            if (err) {
                console.log(err);
            }
            cb && cb(err, results.user);
        });
};

var authenticateWithTwitter = function (consumerKey, consumerSecret, accessTokenKey, accessTokenSecret, cb) {
    var client = new Twitter({
        consumer_key: consumerKey,
        consumer_secret: consumerSecret,
        access_token_key: accessTokenKey,
        access_token_secret: accessTokenSecret
    });

    flow()
        .seq("twitterUser", function (cb) {
            client.get('account/verify_credentials', {
                "include_email": true,
                "skip_status": true
            }, function (err, user, response) {
                if (err) {
                    console.log(response);
                }
                cb(err, user);
            })
        })
        .seq("user", function (cb) {
            if (this.vars.twitterUser.email) {
                User.findOne({'email': this.vars.twitterUser.email}, function (err, user) {
                    cb(null, user);
                })
            } else {
                User.findOne({'oauth.twitter.id': this.vars.twitterUser.id}, function (err, user) {
                    cb(null, user);
                });
            }
        })
        .seq("user", function () {
            var twitterUser = this.vars.twitterUser;
            var twitterAuth = {
                id: twitterUser.id
            };
            var user = this.vars.user;
            if (!user) {
                user = new User({
                    email: twitterUser.email,
                    username: twitterUser.screen_name,
                    name: twitterUser.name,
                    state: User.STATE.NEW
                });
            }
            var oauth = user.oauth || {};
            if (!oauth.twitter) {
                oauth.twitter = twitterAuth;
            }
            user.roles = ["customer"];
            user.oauth = oauth;
            user.profileImage = twitterUser.profile_image_url_https;
            return user;
        })
        .seq(function (cb) {
            if (this.vars.user.state == User.STATE.NEW) {
                setUniqueUsername(this.vars.user, cb);
            } else {
                cb();
            }
        })
        .seq("user", function (cb) {
            this.vars.user.save(cb);
        })
        .exec(function (err, results) {
            if (err) {
                console.log(err);
            }
            cb && cb(err, results.user);
        });


};

module.exports = {
    create: {
        auth: false,
        handler: function (request, reply) {
            var auth = request.payload.auth;

            flow()
                .seq("user", function (cb) {
                    // authenticate
                    if (auth && auth.type == "fb") {
                        authenticateWithFB(auth.accessToken, cb);
                    } else if (auth && auth.type == "test") {
                        authenticateForTest(request.payload, cb);
                    } else if (auth && auth.type == "twitter") {
                        var config = request.server.settings.app.config;
                        authenticateWithTwitter(config.twitter.consumerKey, config.twitter.consumerSecret, auth.tokenKey, auth.tokenSecret, cb);
                    } else {
                        cb("AUTHENTICATION FAILED");
                    }
                })
                .seq("session", function (cb) {
                    var session = new Session({
                        user: this.vars.user.id,
                        auth: auth
                    });
                    session.save(cb);
                })
                .exec(function (err, results) {
                    if (!err) {
                        reply({
                            id: results.session._id,
                            user: {
                                id: "me",
                                state: results.user.state,
                                username: results.user.username,
                                profileImage: results.user.profileImage
                            },
                            auth: auth
                        }).created('/sessions/' + results.session._id); // HTTP 201
                    } else {
                        if (11000 === err.code || 11001 === err.code) {
                            reply(Boom.forbidden("please provide another session id, it already exist"));
                        } else reply(Boom.forbidden(err)); // HTTP 403
                    }
                })

        }
    },

    checkUsername: {
        auth: false,
        handler: function (request, reply) {
            var username = request.payload.username;
            flow()
                .seq("session", function (cb) {
                    Session.findOne({
                        _id: request.headers["session-token"]
                    }, function (err, session) {
                        if (!err && !session) {
                            err = Boom.forbidden("No session found");
                        }
                        cb(err, session);
                    });
                })
                .seq("available", function (cb) {
                    isUsernameAvailable(username, cb);
                })
                .exec(function (err, results) {
                    if (!err) {
                        reply({username: username, available: results.available});
                    } else {
                        reply(err);
                    }
                })
        }
    },

    register: {
        auth: false,
        validate: {
            payload: {
                "username": Joi.string()
            }
        },
        handler: function (request, reply) {
            var username = request.payload.username;
            flow()
                .seq("session", function (cb) {
                    Session.findOne({
                        _id: request.headers["session-token"]
                    }, function (err, session) {
                        if (!err && !session) {
                            err = Boom.forbidden("No session found");
                        }
                        cb(err, session);
                    });
                })
                .seq("user", function (cb) {
                    User.findOne({
                        _id: this.vars.session.user,
                        state: User.STATE.NEW
                    }, function (err, user) {
                        if (!err && !user) {
                            err = Boom.forbidden("No user found for session");
                        }
                        cb(err, user);
                    })
                })
                .seq("available", function (cb) {
                    isUsernameAvailable(username, cb);
                })
                .seq("user", function (cb) {
                    if (this.vars.available) {
                        var user = this.vars.user;

                        user.username = username;
                        user.state = User.STATE.REGISTERED;
                        user.save(cb);
                    } else {
                        cb(Boom.badRequest("Username not available"));
                    }
                })
                .exec(function (err, results) {
                    if (!err) {
                        reply({
                            id: results.user.id,
                            status: User.STATE.REGISTERED,
                            username: results.user.username
                        }).created('/users/' + results.user.username); // HTTP 201
                    } else {
                        reply(err);
                    }
                })
        }
    },

    getOne: {
        auth: false,
        handler: function (request, reply) {
            Session.findOne({
                '_id': request.params.id
            }, 'id user').populate('user').exec(function (err, session) {
                if (!err) {
                    reply(session);
                } else {
                    reply(Boom.badImplementation(err)); // 500 error
                }
            });
        }
    },

    remove: {
        handler: function (request, reply) {
            flow()
//                .seq(function (cb) {
//                    var oauth = request.auth.artifacts.auth;
//                    if (oauth && oauth.type == "fb") {
//                        FB.api("/logout", {accessToken: oauth.accessToken}, function (res) {
//                            if (!res || res.error) {
//                                cb(!res ? 'FB_ERROR_OCCURED' : res.error);
//                            } else {
//                                cb(null, res)
//                            }
//                        });
//                    } else {
//                        cb(null);
//                    }
//                })
                .seq(function (cb) {
                    Session.remove({_id: request.auth.artifacts.sessionId}, cb)
                })
                .exec(function (err) {
                    if (!err) {
                        reply({});
                    } else {
                        reply(Boom.badImplementation(err)); // 500 error
                    }
                })
        }
    }

};