var Joi = require('joi'),
    Boom = require('boom'),
    Design = require('../models/Design'),
    cloudinary = require("cloudinary"),
    _ = require('lodash');


var addResources = function (req) {

    var options = null;
    if(req.headers["image-render-secret"] == "blublablub"){
        options = {};
        _.extend(options, req.server.settings.app.config.cloudinary);
    }



    return function (design) {
        var ret = design.toJSON();

        delete ret._id;
        var baseUrl = "http://res.cloudinary.com/drbxi29nk/image/private/";
        var resources = {};
        resources.SMALL = baseUrl + "c_scale,q_90,w_150/images/" + ret.id + ".jpg";
        resources.SCREEN = baseUrl + "c_scale,q_90,w_800/images/" + ret.id + ".jpg";


        if(options){
            try {
                var privateUrl = cloudinary.utils.private_download_url("images/" + ret.id, "jpg", options);
                resources.ORIGINAL = privateUrl;
            } catch (e) {
                console.log(e);
            }
        }

        ret.resources = resources;

        return ret;
    };
};

module.exports = {
    getAll: {
        handler: function (request, reply) {
            Design.find({}, function (err, designs) {
                if (!err) {
                    reply(_.map(designs, addResources(request)));
                } else {
                    reply(Boom.badImplementation(err)); // 500 error
                }
            });
        }
    },

    create: {
        handler: function (request, reply) {
            var design = new Design(request.payload);
            design.creator = request.auth.credentials.id;
            design.save(function (err, design) {
                if (!err) {
                    reply({
                        id: design._id
                    }).created('/designs/' + design._id); // HTTP 201
                } else {
                    if (11000 === err.code || 11001 === err.code) {
                        reply(Boom.forbidden("please provide another design id, it already exist"));
                    } else reply(Boom.forbidden(err)); // HTTP 403
                }
            });
        }
    },

    getOne: {
        auth: false,
        handler: function (request, reply) {
            Design.findOne({
                '_id': request.params.id
            }, function (err, design) {
                if (!err) {
                    reply(addResources(request)(design));
                } else {
                    reply(Boom.badImplementation(err)); // 500 error
                }
            });
        }
    },

    update: {
        handler: function () {

        }
    }

};