var cloudinary = require('cloudinary'),
    _ = require('lodash');

module.exports = {
    create: {
        handler: function (req, reply) {
            var options = req.payload || {};

            _.extend(options, req.server.settings.app.config.cloudinary);

            var params = cloudinary.uploader.upload_tag_params(options);

            reply(JSON.parse(params)).code(200);

        }
    }
};