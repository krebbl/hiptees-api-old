var Joi = require('joi'),
    Boom = require('boom'),
    Feedback = require('../models/Feedback');

module.exports = {

    create: {
        handler: function (request, reply) {
            var feedback = new Feedback({
                text: request.payload.text
            });
            feedback.author = request.auth.credentials.id;

            feedback.save(function (err, feedback) {
                if (!err) {
                    reply({
                        id: feedback._id
                    }).created('/feedbacks/' + feedback._id); // HTTP 201
                } else {
                    reply(Boom.forbidden(err)); // HTTP 403
                }
            });
        }
    }

};