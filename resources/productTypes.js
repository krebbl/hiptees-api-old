var Joi = require('joi'),
    Boom = require('boom'),
    ProductType = require('../models/ProductType'),
    resolveHost = require('../helpers/resolveHost');

var addResource = function (req) {
    var uri = "api/v1";
    var host = resolveHost(req);
    uri = host + "/" + uri;

    return function (productType) {
        productType.id = productType._id;

        delete productType._id;

        productType.appearances.forEach(function (appearance) {
            var imageId = productType.id + "-" + appearance.id;

            //appearance.resources = {
            //    SMALL: "http://res.cloudinary.com/drbxi29nk/image/upload/c_scale,w_300/product-types/" + imageId + ".jpg",
            //    MEDIUM: "http://res.cloudinary.com/drbxi29nk/image/upload/c_scale,w_600/product-types/" + imageId + ".jpg",
            //    LARGE: "http://res.cloudinary.com/drbxi29nk/image/upload/product-types/" + imageId + ".jpg"
            //};

            appearance.resources = {
                SMALL: uri + "/images/productTypes/" + imageId + ".jpg",
                MEDIUM: uri + "/images/productTypes/" + imageId + ".jpg",
                LARGE: uri + "/images/productTypes/" + imageId + ".jpg"
            };
        });

        return productType;
    }
};


module.exports = {
    getAll: {
        auth: false,

        handler: function (request, reply) {

            ProductType.find({}).lean().exec(function (err, productTypes) {
                if (!err) {
                    productTypes.forEach(addResource(request));
                    reply(productTypes);
                } else {
                    reply(Boom.badImplementation(err)); // 500 error
                }
            });
        }
    },

    create: {
        handler: function () {

        }
    },

    getOne: {
        auth: false,
        handler: function (request, reply) {
            ProductType.findOne({
                '_id': request.params.id
            }).lean().exec(function (err, productType) {
                if (!err) {
                    var f = addResource(request);
                    f(productType);
                    reply(productType);
                } else {
                    reply(Boom.badImplementation(err)); // 500 error
                }
            });
        }
    },

    update: {
        handler: function () {

        }
    },
    helper: {
        toResource: addResource

    }

};