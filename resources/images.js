var ImageRenderer = require('../util/ImageRenderer'),
    Boom = require('boom'),
    fs = require('fs');

module.exports = {
    config: function (baseUri) {
        config.baseUri = baseUri;
    },
    renderProductImages: function (id, force, cb) {
        ImageRenderer.renderProduct(id, force, cb);
    },
    products: {
        auth: false,
        handler: function (request, reply) {
            var id = request.params.id,
                size = parseInt(request.params.size);

            if (ImageRenderer.sizes.indexOf(size) == -1) {
                reply(Boom.badRequest("Size now allowed"));
                return;
            }

            var filePath = ImageRenderer.productImagePath(id, size);

            fs.exists(filePath, function (exists) {
                if (exists) {
                    reply.file(filePath);
                } else {
                    ImageRenderer.renderProduct(id, function (err) {
                        if (err) {
                            reply(Boom.badImplementation(err));
                        } else {
                            reply.file(filePath);
                        }
                    });
                }
            });
        }

    },
    printouts: {
        auth: false,
        handler: function (request, reply) {
            var id = request.params.id;

            var filePath = ImageRenderer.printoutImagePath(id);

            fs.exists(filePath, function (exists) {
                if (exists) {
                    reply.file(filePath);
                } else {
                    ImageRenderer.renderPrintout(id, function (err) {
                        if (err) {
                            reply(Boom.badImplementation(err));
                        } else {
                            reply.file(filePath);
                        }
                    });
                }
            });
        }

    }


};