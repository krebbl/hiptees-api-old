var Joi = require('joi'),
    Boom = require('boom'),
    Product = require('../models/Product'),
    ProductType = require('../models/ProductType'),
    User = require('../models/User'),
    flow = require('flow.js').flow,
    images = require('./images'),
    im = require('imagemagick'),
    resolveHost = require('../helpers/resolveHost');

var addResourceFnc = function (req) {
    var uri = "api/v1";
    var host = resolveHost(req);
    uri = host + "/" + uri;

    return function (product) {
        var p = product.toJSON();

        p.id = p._id;

        if ((product.productType instanceof ProductType)) {
            p.productType.id = product.productType._id;
            delete p.productType._id;
        } else {
            p.productType = {
                id: p.productType
            };
        }


        delete p._id;

        if(p.creator){
            if (p.creator.constructor.name == "ObjectID") {
                p.creator = {
                    id: p.creator.toString()
                };
            } else {
                p.creator = {
                    id: p.creator.id,
                    username: p.creator.username
                }
            }

        }

        p.href = uri + "/products/" + product.id;

        p.resources = {
            "LARGE": uri + "/images/products/" + product.id + "-900.png",
            "MEDIUM": uri + "/images/products/" + product.id + "-600.png",
            "SMALL": uri + "/images/products/" + product.id + "-300.png"
        };

        if(p.creator.username){
            // TODO: set this via config
            p.shareLink = "http://127.0.0.1:8080/"+ p.creator.username + "/" + p.id;
        }

        return p;
    };
};

var queryProducts = function (where, offset, limit, request, reply) {
    flow()
        .seq("count", function (cb) {
            Product.count(where).exec(cb);
        })
        .seq("products", function (cb) {
            Product.find(where).select('id name creator productType appearance state __v').skip(offset).limit(limit).sort('-updatedAt').exec(cb);
        })
        .seq("products", function () {
            var products = this.vars.products;
            return products.map(addResourceFnc(request));
        })
        .exec(function (err, result) {
            if (!err) {
                reply({
                    results: result.products,
                    limit: limit,
                    offset: offset,
                    total: result.count
                });
            } else {
                reply(Boom.badImplementation(err)); // 500 error
            }
        });
};

var checkProductTypeAndAppearance = function (productTypeId, appearanceId, cb) {
    ProductType.findOne({
        _id: productTypeId
    }, function (err, productType) {
        if (!productType) {
            err = Boom.badRequest("product type not found");
        } else {
            var appearances = productType.appearances.filter(function (appearance) {
                return appearance.id == appearanceId;
            });
            if (appearances.length == 0) {
                err = Boom.badRequest("wrong appearance id");
            }
        }

        cb(err);
    })
};

var loadProduct = function (id, user, cb) {
    Product.findOne({
        '_id': id
    }).populate("creator").exec(function (err, product) {
        if (!err) {
            if (!product) {
                err = Boom.notFound("product not found");
            } else if (product.creator.id !== user.id) {
                err = Boom.forbidden("you are not the creator of this product");
            }
        }

        cb(err, product);
    });
};

var queryUserProducts = function (userId, state, request, reply) {
    var query = {};

    var where = {state: state};
    if (query.productType) {
        where["productType"] = query.productType;
    }

    if (query.appearance) {
        where["appearance.id"] = query.appearance;
    }

    where["creator"] = userId;

    var limit = parseInt(query.limit) || 100,
        offset = parseInt(query.offset) || 0;

    queryProducts(where, offset, limit, request, reply);
};

var PayloadSchema = {
    "name": Joi.string().empty(''),
    "configurations": Joi.array().default([]),
    "productType": Joi.object({
        id: Joi.string()
    }),
    "appearance": Joi.object({
        id: Joi.string()
    }),
    "state": Joi.string().default('draft'),
    "tags": Joi.array().default(["preset"])
};


module.exports = {
    getAll: {
        auth: false,
        handler: function (request, reply) {
            var query = request.query;
            var where = {
//                published: true
            };
            if (query.tags) {
                where.tags = {$in: query.tags.split(",")}
            }
            if (query.productType) {
                where["productType"] = query.productType;
            }

            if (query.appearance) {
                where["appearance.id"] = query.appearance;
            }

            if (query.user) {
                where["creator"] = query.user;
            }

            var limit = parseInt(query.limit) || 100,
                offset = parseInt(query.offset) || 0;

            queryProducts(where, offset, limit, request, reply);
        }
    },

    create: {
        validate: {
            payload: PayloadSchema
        },
        handler: function (req, reply) {
            var payload = req.payload;
            var product = new Product(payload);

            product.creator = req.auth.credentials.id;

            var allowedStates = Product.getAllowedStates();

            if (allowedStates.indexOf(product.state) === -1) {
                product.state = Product.STATE.DRAFT;
            }

            flow()
                .seq(function (cb) {
                    checkProductTypeAndAppearance(payload.productType.id, payload.appearance.id, cb);
                })
                .seq("product", function (cb) {
                    product.productType = payload.productType.id;

                    product.save(cb);
                })
                .seq(function () {
                    images.renderProductImages(product.id, true);
//                    createProductImages(this.vars.product.id, function (err) {
//                        console.log(err);
//                    });
                })
                .exec(function (err, results) {
                    if (!err) {
                        reply(addResourceFnc(req)(results.product)).created('/products/' + product.id); // HTTP 201
                    } else {
                        if (11000 === err.code || 11001 === err.code) {
                            reply(Boom.forbidden("please provide another product id, it already exist"));
                        } else reply(err); // HTTP 403
                    }
                });
        }
    },

    getOne: {
        auth: false,
        handler: function (request, reply) {
            Product.findOne({
                '_id': request.params.id
            }).populate('creator', "id username").exec(function (err, product) {
                if (!err) {
                    if (!product) {
                        reply(Boom.notFound("product not found"));
                    } else {
                        reply(addResourceFnc(request)(product));
                    }
                } else {
                    reply(Boom.badImplementation(err)); // 500 error
                }
            });
        }
    },

    update: {
        validate: {
            payload: PayloadSchema
        },
        handler: function (request, reply) {
            var user = request.auth.credentials,
                payload = request.payload;

            flow()
                .seq("product", function (cb) {
                    loadProduct(request.params.id, user, cb);
                })
                .seq(function () {
                    if (this.vars.product.state !== Product.STATE.DRAFT) {
                        throw Boom.badRequest("product can't be updated anymore");
                    }
                })
                .seq(function (cb) {
                    checkProductTypeAndAppearance(payload.productType.id, payload.appearance.id, cb);
                })
                .seq(function (cb) {
                    var product = this.vars.product;
                    product.set(request.payload);

                    product.productType = payload.productType.id;
                    product.creator = user.id;

                    var allowedStates = Product.getAllowedStates();

                    if (allowedStates.indexOf(product.state) === -1) {
                        product.state = Product.STATE.DRAFT;
                    }

                    product.save(cb);
                })
                .seq(function () {
                    var product = this.vars.product;
                    images.renderProductImages(product.id, true);
//                    createProductImages(product.id);
                })
                .exec(function (err, results) {
                    if (!err) {
                        reply(addResourceFnc(request)(results.product)).code(200); // HTTP 200
                    } else {
                        if (11000 === err.code || 11001 === err.code) {
                            reply(Boom.forbidden("please provide another product id, it already exist"));
                        } else {
                            reply(err)
                        }
                    }
                })
        }
    },

    patch: {
        validate: {
            payload: {
                state: Joi.string()
            }
        },
        handler: function (req, reply) {
            flow()
                .seq("product", function (cb) {
                    loadProduct(req.params.id, req.auth.credentials, cb);
                })
                .seq("product", function (cb) {
                    var product = this.vars.product;

                    if (!product.isStateAllowed(req.payload.state)) {
                        throw Boom.badRequest("invalid state change");
                    }

                    product.state = req.payload.state;

                    product.save(cb);
                })
                .exec(function (err, results) {

                    if (err) {
                        reply(err);
                    } else {
                        reply(addResourceFnc(req)(results.product)).code(200);
                    }

                });
        }
    },

    updateState: {
        handler: function (req, reply) {
            flow()
                .seq("product", function (cb) {
                    loadProduct(req.payload.product.id, req.auth.credentials, cb);
                })
                .seq("product", function (cb) {
                    var product = this.vars.product;

                    if (!product.isStateAllowed(req.payload.state)) {
                        throw Boom.badRequest("invalid state change");
                    }

                    product.state = req.payload.state;

                    product.save(cb);
                })
                .exec(function (err, results) {
                    if (err) {
                        reply(err);
                    } else {
                        reply(addResourceFnc(req)(results.product)).code(200);
                    }

                });
        }
    },

    userDrafts: {
        handler: function (request, reply) {
            var user = request.auth.credentials;
            queryUserProducts(user.id, Product.STATE.DRAFT, request, reply);
        }
    },

    userPublished: {
        auth: {
            mode: "try",
            strategies: ["session-required"]
        },
        handler: function (request, reply) {
            flow()
                .seq("user", function (cb) {
                    if (request.params.username) {
                        User.findOne({
                            username: request.params.username
                        }, function (err, user) {
                            if (!user) {
                                err = Boom.notFound("user not found");
                            }
                            cb(err, user);
                        })
                    } else if (request.auth && request.auth.credentials) {
                        var user = request.auth.credentials;
                        cb(null, user);
                    } else {
                        cb(Boom.notFound("user not found"));
                    }
                })
                .exec(function (err, results) {
                    if (!err) {
                        queryUserProducts(results.user.id, Product.STATE.PUBLIC, request, reply);
                    } else {
                        reply(err);
                    }
                })

        }

    },

    userPrivate: {
        handler: function (request, reply) {
            var user = request.auth.credentials;
            queryUserProducts(user.id, Product.STATE.PRIVATE, request, reply);
        }
    },

    helper: {
        toResource: addResourceFnc
    }

};