var PORT = process.env.PORT || 3000;
var ENV = process.env.NODE_ENV || 'dev';

// setup http + express + socket.io
var Api = require('./api');

Api.start(ENV, function () {
    console.log("API started");

    //Api.loadFixtures();
});