module.exports = function(request){
    var host = "";
    if (request.headers["referer"]) {
        var match = request.headers["referer"].match(/https?:\/\/[^\/]+/);
        if(match){
            return match[0];
        }
        return request.server.info.uri;
    } else {
        host = request.server.info.uri;
    }
    return host;
};