module.exports = {
    server: {
        host: '0.0.0.0',
        port: 3000,
//        uri: "http://192.168.178.134/hip/api/v1"
//        uri: "http://localhost/hip/api/v1"
        uri: "http://app.hiptees.com/api/v1"
    },
    database: {
        host: '127.0.0.1',
        port: 27017,
        db: 'test-app'
    },
    spreadshirt: {
        shop: "783914",
        apiKey: "INSERT",
        secret: "INSERT",
        apiEndpoint: "http://api.spreadshirt.de/api/v1",
        imageEndpoint: "http://image.spreadshirt.de/image-server/v1"
    },
    cloudinary: {
        api_key: "INSERT",
        api_secret: "INSERT"
    }

};