var mongoose = require('mongoose');

var ObjectId = require('mongoose').mongo.ObjectID;

module.exports = {

    productTypes: [
        {
            "_id": ObjectId("55c28229f78e2d64819b2a75"),
            "appearances": [
                {
                    "name": "white",
                    "id": "1",
                    "color": "#FFFFFF"
                },
                {
                    "name": "black",
                    "id": "2",
                    "color": "#000000"
                }],
            "name": "Men T-Shirt",
            "printArea": {
                "offset": {
                    "x": 252.213,
                    "y": 130
                },
                "size": {
                    "width": 308.0549199062,
                    "height": 450
                }
            },
            "size": {
                "width": 813.73,
                "height": 813.73
            },
            "sprdId": "6",
            "sizes": [
                {
                    "name": "S",
                    "group": "S",
                    "measures": [
                        {
                            "name": "A",
                            "value": {
                                "value": 700,
                                "unit": "mm"
                            }
                        },
                        {
                            "name": "B",
                            "value": {
                                "value": 500,
                                "unit": "mm"
                            }
                        }
                    ],
                    "id": "2"
                },
                {
                    "name": "M",
                    "group": "M",
                    "measures": [
                        {
                            "name": "A",
                            "value": {
                                "value": 715,
                                "unit": "mm"
                            }
                        },
                        {
                            "name": "B",
                            "value": {
                                "value": 530,
                                "unit": "mm"
                            }
                        }
                    ],
                    "id": "3"
                },
                {
                    "name": "L",
                    "group": "L",
                    "measures": [
                        {
                            "name": "A",
                            "value": {
                                "value": 730,
                                "unit": "mm"
                            }
                        },
                        {
                            "name": "B",
                            "value": {
                                "value": 550,
                                "unit": "mm"
                            }
                        }
                    ],
                    "id": "4"
                },
                {
                    "name": "XL",
                    "group": "XL",
                    "measures": [
                        {
                            "name": "A",
                            "value": {
                                "value": 740,
                                "unit": "mm"
                            }
                        },
                        {
                            "name": "B",
                            "value": {
                                "value": 590,
                                "unit": "mm"
                            }
                        }
                    ],
                    "id": "5"
                },
                {
                    "name": "XXL",
                    "group": "XXL",
                    "measures": [
                        {
                            "name": "A",
                            "value": {
                                "value": 780,
                                "unit": "mm"
                            }
                        },
                        {
                            "name": "B",
                            "value": {
                                "value": 620,
                                "unit": "mm"
                            }
                        }
                    ],
                    "id": "6"
                },
                {
                    "name": "3XL",
                    "group": "3XL",
                    "measures": [
                        {
                            "name": "A",
                            "value": {
                                "value": 795,
                                "unit": "mm"
                            }
                        },
                        {
                            "name": "B",
                            "value": {
                                "value": 660,
                                "unit": "mm"
                            }
                        }
                    ],
                    "id": "38"
                }
            ]
        }
    ],
    products: [{
        "_id": ObjectId("560fd4c24790082f0e3a0ea3"),
        "productType": ObjectId("55c28229f78e2d64819b2a75"),
        "name": "White shirt",
        "configurations": [],
        "appearance": {id: "1"},
        "creator": null,
        "resourceProvider": "cloudinary",
        "state": "published",
        "tags": ["preset"]
    },
        {
            "_id": ObjectId("560fd4c24790082f0e3a0ea4"),
            "productType": ObjectId("55c28229f78e2d64819b2a75"),
            "name": "White shirt",
            "configurations": [],
            "appearance": {id: "2"},
            "creator": null,
            "resourceProvider": "cloudinary",
            "state": "published",
            "tags": ["preset"]
        }],
    users: [{
        "_id": ObjectId("5614d54576c8677c176d8c05"),
        "email": "krebbl2@gmail.com",
        "name": "Test Customer",
        "roles": ["customer"],
        "__v": 0,
        "state": "registered",
        "username": "krebbl"
    }],
    sessions: [],
    baskets: []
};