var PORT = process.env.PORT || 3001;
var ENV = process.env.NODE_ENV || 'test';

// setup http + express + socket.io
var Api = require('./api');

Api.start(ENV, function () {
    Api.loadFixtures();
});

//app.post("/cloudinaryFormData", jsonParser, function (req, res) {
//    var options = req.body || {};
//
//    _.extend(options, config.cloudinary);
//
//    var params = cloudinary.uploader.upload_tag_params(options);
//
//    res.writeHead(200, {'Content-Type': 'application/json'});
//    res.end(params);
//});