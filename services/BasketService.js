var flow = require('flow.js').flow,
    Basket = require('../models/Basket');


module.exports = {
    inject: {

    },
    checkProductAndSize: function(productId, sizeId, cb){

    },
    addItem: function(id, basketItem, cb){

        var basketId = id,
            productId = basketItem.product.id,
            quantity = basketItem.quantity,
            sizeId = basketItem.size.id,
            self = this;

        flow()
            .seq("basket", function (cb) {
                if (basketId) {
                    Basket.findOne({
                        _id: basketId
                    }, function (err, basket) {
                        if (!basket) {
                            basket = new Basket();
                        }
                        cb(null, basket);
                    });
                } else {
                    var basket = new Basket();
                    cb(null, basket);
                }
            })
            .seq(function (cb) {
                self.checkProductAndSize(productId, sizeId, cb);
            })
            .seq("savedBasket", function (cb) {
                var basketItems = this.vars.basket.items;
                var basketItem = null;

                for (var i = 0; i < basketItems.length; i++) {
                    var item = basketItems[i];
                    if (item.product.toString() == productId && item.size.id == sizeId) {
                        basketItem = item;
                        break;
                    }
                }

                if (basketItem) {
                    basketItem.quantity += quantity;
                } else {
                    basketItems.push({
                        product: productId,
                        quantity: quantity,
                        "size.id": sizeId
                    });
                }


                this.vars.basket.save(cb);
            })
            .seq("payload", function (cb) {
                buildCombinedBasket(request, this.vars.savedBasket, cb);
            })
            .exec(function (err, results) {
                if (!err) {
                    reply(results.payload).code(200);
                } else {
                    reply(err);
                }
            });
    },
    removeItem: function(id, basketItemId, cb){

    },
    loadBasket: function(id){

    }



};