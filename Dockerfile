FROM    centos:centos6

# Enable EPEL for Node.js
RUN     rpm -Uvh http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm

# Install phantomjs
RUN     yum -y install gcc gcc-c++ make flex bison gperf ruby \
  openssl-devel freetype-devel fontconfig-devel libicu-devel sqlite-devel \
  libpng-devel libjpeg-devel

RUN     yum -y install git

RUN     git clone git://github.com/ariya/phantomjs.git
RUN     cd phantomjs; git checkout 2.0; yes "y" | ./build.sh

RUN     cd phantomjs; mv bin/phantomjs /usr/local/bin/.

# Install Node.js and npm
RUN     yum install -y npm

# Bundle app source
COPY . /src

# TODO: copy hiptees app!


# Install app dependencies
RUN cd /src; npm install

EXPOSE  3000

CMD ["node", "/src/server.js"]