/**
 * User: mkre
 * Date: 07.09.15
 * Time: 14:09
 */
var request = require('request'),
    _ = require('lodash'),
    QueryString = require('querystring'),
    expect = require('chai').expect,
//    uuid = require('node-uuid'),
    flow = require('flow.js').flow,
    util = require('./util'),
    config = require('../env/test.js');


describe('API baskets', function () {

    var authenticatedRequest,
        baseRequest;

    before(function (done) {
        flow()
            .seq("authRequest", function (cb) {
                util.createAuthRequest(cb);
            })
            .seq("baseRequest", function (cb) {
                util.createBaseRequest(cb);
            })
            .exec(function (err, results) {
                authenticatedRequest = results.authRequest;
                baseRequest = results.baseRequest;
                done(err);
            })

    });
// login and create default request


    after(function () {
        // stop server
    });


    function addBasketItem(payload, cb) {
        baseRequest.post(
            {
                uri: "/addBasketItem",
                body: payload
            }, cb)
    }

    function updateBasketItem(payload, cb) {
        baseRequest.post({
            uri: "/updateBasketItem",
            body: payload
        }, cb);
    }

    function removeBasketItem(payload, cb) {
        baseRequest.post({
            uri: "/removeBasketItem",
            body: payload
        }, cb);
    }

    describe('# GET /baskets/{id}', function () {

        it('should return basket', function (done) {
            flow()
                .seq("basket", function (cb) {
                    addBasketItem({
                        "product": {
                            "id": "560fd4c24790082f0e3a0ea3"
                        },
                        "size": {
                            "id": "2"
                        },
                        "quantity": 1
                    }, function (err, response, body) {
                        cb(err, body.basket);
                    });
                })
                .exec(function (err, results) {
                    baseRequest.get({
                        "uri": "/baskets/" + results.basket.id
                    }, function (err, response, body) {
                        expect(err).to.not.exist;
                        expect(response.statusCode).to.eql(200);

                        done(err, body);
                    });
                });
        });

        it('should return 404 if not found', function (done) {
            baseRequest.get({
                "uri": "/baskets/" + "12312321"
            }, function (err, response, body) {
                expect(err).to.not.exist;
                expect(response.statusCode).to.eql(404);

                done(err, body);
            });
        })

    });

    describe('# POST /addBasketItem', function () {

        it('should create a basket if no basket id is set', function (done) {
            addBasketItem({
                "product": {
                    "id": "560fd4c24790082f0e3a0ea3"
                },
                "size": {
                    "id": "2"
                },
                "quantity": 1
            }, function (err, response, body) {

                expect(response.statusCode).to.equal(200, "wrong status code");
                expect(body).to.have.property("basket");
                expect(body).to.have.property("products");
                expect(body).to.have.property("productTypes");


                var basket = body.basket;
                expect(basket).to.have.property("id");
                expect(basket).to.have.property("totalPrice");
                expect(basket).to.have.property("items");

                var item = basket.items[0];
                expect(item).to.have.property("id");
                expect(item).to.have.property("quantity");
                expect(item).to.have.property("price");
                expect(item).to.have.property("product");
                expect(item).to.have.property("size");

                done(err);
            });
        });

        it('should return basket with basket id', function (done) {
            flow()
                .seq("basket", function (cb) {
                    addBasketItem({
                        "product": {
                            "id": "560fd4c24790082f0e3a0ea3"
                        },
                        "size": {
                            "id": "2"
                        },
                        "quantity": 1
                    }, function (err, response, body) {
                        cb(err, body);
                    });
                }).seq("basket2", function (cb) {
                    addBasketItem({
                        "basket": {
                            "id": this.vars.basket.id
                        },
                        "product": {
                            "id": "560fd4c24790082f0e3a0ea3"
                        },
                        "size": {
                            "id": "2"
                        },
                        "quantity": 1
                    }, function (err, response, body) {
                        cb(err, body);
                    });
                }).exec(function (err, results) {
                    expect(results.basket.id).to.eql(results.basket2.id);

                    done(err);
                })
        });

        it('should merge items with same size and product id', function (done) {
            var addQuantity = 4;
            flow()
                .seq("basket", function (cb) {
                    addBasketItem({
                        "product": {
                            "id": "560fd4c24790082f0e3a0ea3"
                        },
                        "size": {
                            "id": "2"
                        },
                        "quantity": 1
                    }, function (err, response, body) {
                        cb(err, body.basket);
                    });
                }).seq("basket2", function (cb) {
                    addBasketItem({
                        "basket": {
                            "id": this.vars.basket.id
                        },
                        "product": {
                            "id": "560fd4c24790082f0e3a0ea3"
                        },
                        "size": {
                            "id": "2"
                        },
                        "quantity": addQuantity
                    }, function (err, response, body) {
                        cb(err, body.basket);
                    });
                }).exec(function (err, results) {
                    expect(results.basket.id).to.eql(results.basket2.id);
                    expect(results.basket.items.length).to.eql(results.basket2.items.length);
                    expect(results.basket.items[0].quantity + addQuantity).to.eql(results.basket2.items[0].quantity);

                    done(err);
                })
        });

        it('should return 400 if no quantity is set', function (done) {
            addBasketItem({
                "product": {
                    "id": "560fd4c24790082f0e3a0ea3"
                },
                "size": {
                    "id": "2"
                }
            }, function (err, body, response) {
                expect(response.statusCode).to.equal(400, "wrong status code");

                done(err);
            });
        });

        it('should return 400 if no size is set', function (done) {
            addBasketItem({
                "product": {
                    "id": "560fd4c24790082f0e3a0ea3"
                },
                "quantity": 1
            }, function (err, body, response) {
                expect(response.statusCode).to.equal(400, "wrong status code");

                done(err);
            });
        });

        it('should return 400 if wrong size is set', function (done) {
            addBasketItem({
                "product": {
                    "id": "560fd4c24790082f0e3a0ea3"
                },
                "size": {
                    "id": "0"
                },
                "quantity": 1
            }, function (err, body, response) {
                expect(response.statusCode).to.equal(400, "wrong status code");

                done(err);
            });
        });

        it('should return 400 if wrong product id is set', function (done) {
            addBasketItem({
                "product": {
                    "id": "12"
                },
                "size": {
                    "id": "2"
                },
                "quantity": 1
            }, function (err, body, response) {
                expect(response.statusCode).to.equal(400, "wrong status code");

                done(err);
            });
        });

    });


    describe('# POST /removeBasketItem', function () {
        it('should remove item from basket', function (done) {
            flow()
                .seq("basket", function (cb) {
                    addBasketItem({
                        "product": {
                            "id": "560fd4c24790082f0e3a0ea3"
                        },
                        "size": {
                            "id": "2"
                        },
                        "quantity": 1
                    }, function (err, response, body) {
                        cb(err, body.basket);
                    });
                })
                .seq("basket2", function (cb) {
                    removeBasketItem({
                        "basket": {
                            "id": this.vars.basket.id
                        },
                        "item": {
                            id: this.vars.basket.items[0].id
                        }
                    }, function (err, response, body) {
                        expect(response.statusCode).to.eql(200);
                        cb(err, body.basket);
                    });
                }).exec(function (err, results) {
                    expect(err).not.to.exist;
                    expect(results.basket.id).to.eql(results.basket2.id);
                    expect(results.basket2.items.length).to.eql(0);

                    done(err);
                })
        });

        it('should throw error when no basket id or basket item id is set', function () {

        });
    });


    describe('# POST /updateBasketItem', function () {

        it('should update size and quantity of basket item', function (done) {
            var newQuantity = 4,
                newSizeId = "3";
            flow()
                .seq("basket", function (cb) {
                    addBasketItem({
                        "product": {
                            "id": "560fd4c24790082f0e3a0ea3"
                        },
                        "size": {
                            "id": "2"
                        },
                        "quantity": 1
                    }, function (err, response, body) {
                        cb(err, body.basket);
                    });
                }).seq("basket2", function (cb) {
                    updateBasketItem({
                        "basket": {
                            "id": this.vars.basket.id
                        },
                        "item": {
                            id: this.vars.basket.items[0].id
                        },
                        "size": {
                            "id": newSizeId
                        },
                        "quantity": newQuantity
                    }, function (err, response, body) {
                        cb(err, body.basket);
                    });
                }).exec(function (err, results) {
                    expect(results.basket.id).to.eql(results.basket2.id);
                    expect(results.basket.items.length).to.eql(results.basket2.items.length);
                    expect(results.basket2.items[0].quantity).to.eql(newQuantity);
                    expect(results.basket2.items[0].size.id).to.eql(newSizeId);

                    done(err);
                })
        });

        it('should return 400 when basket item id is not set', function () {
            flow()
                .seq("basket", function (cb) {
                    addBasketItem({
                        "product": {
                            "id": "560fd4c24790082f0e3a0ea3"
                        },
                        "size": {
                            "id": "2"
                        },
                        "quantity": 1
                    }, function (err, response, body) {
                        cb(err, body);
                    });
                })
                .seq("response", function (cb) {
                    updateBasketItem({
                        "basket": {
                            "id": this.vars.basket.id
                        },
                        "item": {
                            id: "12123123"
                        },
                        "size": {
                            "id": "3"
                        },
                        "quantity": 3
                    }, function (err, response, body) {
                        cb(err, body);
                    });
                }).exec(function (err, results) {
                    expect(results.response.statusCode).to.eql(400);

                    done(err);
                })
        });

        it('should return 400 when size id is wrong', function () {
            flow()
                .seq("basket", function (cb) {
                    addBasketItem({
                        "product": {
                            "id": "560fd4c24790082f0e3a0ea3"
                        },
                        "size": {
                            "id": "2"
                        },
                        "quantity": 1
                    }, function (err, response, body) {
                        cb(err, body);
                    });
                })
                .seq("response", function (cb) {
                    updateBasketItem({
                        "basket": {
                            "id": this.vars.basket.id
                        },
                        "item": {
                            id: this.vars.basket.items[0].id
                        },
                        "size": {
                            "id": "0"
                        },
                        "quantity": 3
                    }, function (err, response, body) {
                        cb(err, body);
                    });
                }).exec(function (err, results) {
                    expect(results.response.statusCode).to.eql(400);

                    done(err);
                })
        });

        it('should merge items with same product id and sizes', function () {
            var items = {
                item1: {
                    "product": {
                        "id": "560fd4c24790082f0e3a0ea3"
                    },
                    "size": {
                        "id": "2"
                    },
                    "quantity": 1
                }, item2: {
                    "product": {
                        "id": "560fd4c24790082f0e3a0ea3"
                    },
                    "size": {
                        "id": "3"
                    },
                    "quantity": 1
                }
            };
            flow()
                .parEach(items, function (item, cb) {
                    addBasketItem(item, function (err, response, body) {
                        cb(err, body);
                    });
                })
                .seq("basket", function (cb) {
                    updateBasketItem({
                        "basket": {
                            "id": this.vars.item1.basket.id
                        },
                        "item": {
                            "id": this.vars.item1.basket.items[0].id
                        },
                        "size": {
                            "id": items.item2.size.id
                        }
                    }, function (err, response, body) {
                        cb(err, body.basket);
                    });
                }).exec(function (err, results) {
                    expect(results.basket.items).to.have.length(1);
                    expect(results.basket.items[0].quantity).to.eql(items.item1.quantity + items.item2.quantity);
                    expect(results.basket.items[0].size.id).to.eql(items.item2.size.id);

                    done(err);
                })
        });
    })


});