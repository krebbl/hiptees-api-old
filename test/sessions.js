/**
 * User: mkre
 * Date: 07.09.15
 * Time: 14:09
 */
var request = require('request'),
    _ = require('lodash'),
    QueryString = require('querystring'),
    expect = require('chai').expect,
//    uuid = require('node-uuid'),
    flow = require('flow.js').flow,
    util = require('./util'),
    config = require('../env/test.js');


describe('API sessions + register', function () {

    var authenticatedRequest,
        baseRequest;

    before(function (done) {
        flow()
            .seq("baseRequest", function (cb) {
                util.createBaseRequest(cb);
            })
            .exec(function (err, results) {
                baseRequest = results.baseRequest;
                done(err);
            })

    });
// login and create default request


    after(function () {
        // stop server
    });


    function createSession(payload, cb) {
        baseRequest.post(
            {
                uri: "/sessions",
                body: payload
            }, cb)
    }

    function registerUser(payload, sessionToken, cb) {
        baseRequest.post({
            uri: "/register",
            body: payload,
            headers: {
                "session-token": sessionToken
            }
        }, cb);
    }

    function checkUsername(username, sessionToken, cb) {
        baseRequest.post({
            uri: "/checkUsername",
            body: {
                username: username
            },
            headers: {
                "session-token": sessionToken
            }
        }, cb);
    }

    function loginAndRegister(email, username, cb) {
        if (username instanceof Function) {
            cb = username;
            username = email.split("@").shift();
        }

        flow()
            .seq("session", function (cb) {
                createSession({
                    email: email,
                    auth: {
                        type: "test"
                    }
                }, function (err, response, body) {
                    cb(err, {
                        response: response,
                        body: body
                    });
                })
            })
            .seq("register", function (cb) {
                registerUser({
                    username: username
                }, this.vars.session.body.id, function (err, response, body) {
                    cb(err, {
                        response: response,
                        body: body
                    });
                })
            })
            .exec(cb);
    }

    describe('# POST /sessions + /register', function () {

        it('should create a new user if login data is valid', function (done) {

            loginAndRegister("newUser@what.de", function (err, results) {

                expect(results.session.response.statusCode).to.equal(201);
                expect(results.session.body.id).to.exist;
                expect(results.session.body.user).to.exist;
                expect(results.session.body.user.state).to.eql("new");
                expect(results.session.body.user.username).to.exist;

                expect(results.register.response.statusCode).to.eql(201);
                expect(results.register.response.headers.location).to.contain("newUser");

                done(err);

            });
        });

        it('should use existing user if login data is valid', function (done) {

            createSession({
                email: "newUser@what.de",
                auth: {
                    type: "test"
                }
            }, function (err, response, body) {
                expect(err).to.not.exist;

                expect(response.statusCode).to.eql(201);
                expect(body.id).to.exist;
                expect(body.user).to.exist;
                expect(body.user.state).to.eql("registered");
                expect(body.user.username).to.eql("newUser");
                done(err);
            })

        });

        it('should return 400 if register is called for registered user', function (done) {

            loginAndRegister("newUser@what.de", function (err, results) {

                expect(err).to.not.exist;

                expect(results.session.response.statusCode).to.eql(201);
                expect(results.session.body.id).to.exist;
                expect(results.session.body.user).to.exist;
                expect(results.session.body.user.state).to.eql("registered");
                expect(results.session.body.user.username).to.eql("newUser");

                expect(results.register.response.statusCode).to.eql(403);

                done(err);

            });

        });

        it('should return 400 when username is already in use', function (done) {

            flow()
                .seq(function (cb) {
                    loginAndRegister("randomHarry@what.de", "randomHarry", cb);
                })
                .seq("results", function (cb) {
                    loginAndRegister("randomHarriesMother@what.de", "randomHarry", cb);
                })
                .exec(function (err, results) {
                    var register = results.results.register;
                    expect(register.response.statusCode).to.eql(400);

                    done(err);
                })

        })
    });

    describe('# POST /checkUsername ', function () {

        it('should return true if username is available', function (done) {
            flow()
                .seq("res", function (cb) {
                    loginAndRegister("checkUsername@what.de", cb);
                })
                .seq("response", function (cb) {
                    checkUsername("checkUsername2", this.vars.res.session.body.id, function (err, response, body) {
                        expect(response.statusCode).to.eql(200);
                        expect(body.available).to.eql(true);
                        cb(err);
                    });
                })
                .exec(function (err) {
                    done(err);
                });
        });

        it('should return false if username is taken', function (done) {
            flow()
                .seq("res", function (cb) {
                    loginAndRegister("checkUsername2@what.de", cb);
                })
                .seq("response", function (cb) {
                    checkUsername("checkUsername2", this.vars.res.session.body.id, function (err, response, body) {
                        expect(response.statusCode).to.eql(200);
                        expect(body.available).to.eql(false);

                        cb(err);
                    });
                })
                .exec(function (err) {
                    done(err);
                });
        });

    })

});