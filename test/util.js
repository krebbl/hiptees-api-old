var request = require('request'),
    flow = require('flow.js').flow,
    config = require('../env/test.js');

module.exports = {
    createAuthRequest: function (done) {
        var authenticatedRequest;
        flow()
            .seq("session", function (cb) {
                request.post(config.server.uri + "/sessions", {
                    json: true,
                    body: {
                        email: "test@hiptees.de",
                        auth: {
                            type: "test"
                        }
                    }
                }, function (err, response, body) {
                    if (!err && response.statusCode == 201) {
                        cb(null, body);
                    } else {
                        cb(err ? err : response.statusMessage);
                    }
                });
            })
            .seq("user", function (cb) {
                request.post(config.server.uri + "/register", {
                    json: true,
                    body: {
                        username: this.vars.session.user.username
                    },
                    'headers': {
                        "session-token": this.vars.session.id
                    }
                }, function (err, response, body) {
                    if (!err && response.statusCode == 201) {
                        cb(null, body);
                    } else {
                        cb(err ? err : response.statusMessage);
                    }
                })
            })
            .exec(function (err, results) {
                if (!err) {
                    authenticatedRequest = request.defaults({
                        'json': true,
                        'baseUrl': config.server.uri,
                        'headers': {
                            "session-token": results.session.id
                        }
                    });
                }

                done(err, authenticatedRequest);
            });
    },
    createBaseRequest: function (done) {
        done(null, request.defaults({
            'json': true,
            'baseUrl': config.server.uri
        }));
    }

};