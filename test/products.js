/**
 * User: mkre
 * Date: 07.09.15
 * Time: 14:09
 */
var request = require('request'),
    _ = require('lodash'),
    QueryString = require('querystring'),
    expect = require('chai').expect,
//    uuid = require('node-uuid'),
    flow = require('flow.js').flow,
    config = require('../env/test.js');


describe('API products', function () {

    var authenticatedRequest,
        baseRequest;

    before(function (done) {
        flow()
            //.seq("api", function (cb) {
            //    // start server
            //    Api.start("test", function (err, server, config, mongoose) {
            //        cb(err, {
            //            config: config,
            //            mongoose: mongoose
            //        });
            //    });
            //
            //})
            //.seq(function (cb) {
            //    // load fixtures
            //    Api.loadFixtures(cb);
            //})
            .seq("session", function (cb) {
                request.post(config.server.uri + "/sessions", {
                    json: true,
                    body: {
                        email: "test@hiptees.de",
                        auth: {
                            type: "test"
                        }
                    }
                }, function (err, response, body) {
                    if (!err && response.statusCode == 201) {
                        cb(null, body);
                    } else {
                        cb(err ? err : response.statusMessage);
                    }
                });
            })
            .exec(function (err, results) {
                if (!err) {
                    baseRequest = request.defaults({
                        'json': true,
                        'baseUrl': config.server.uri
                    });
                    authenticatedRequest = request.defaults({
                        'json': true,
                        'baseUrl': config.server.uri,
                        'headers': {
                            "session-token": results.session.id
                        }
                    });
                }

                done(err);
            });

    });
// login and create default request


    after(function () {
        // stop server
    });

    describe('# GET /products', function () {

        it('should have properties total, results, limit, offset', function (done) {

            baseRequest.get('/products', {}, function (err, response, body) {
                expect(response.statusCode).to.equal(200, "wrong status code");
                expect(body).to.have.property("total");
                expect(body).to.have.property("results");
                expect(body).to.have.property("limit");
                expect(body).to.have.property("offset");

                done();
            });

        });


    });

    function createProduct(payload, cb) {
        authenticatedRequest.post(
            {
                uri: "/products",
                body: payload
            }, function (err, response, body) {
                cb(err, body);
            })
    }

    function createEmptyProduct(state, cb) {
        if (state instanceof Function) {
            cb = state;
            state = null;
        }
        createProduct({
            name: 'Empty Product',
            appearance: {
                id: "1"
            },
            productType: {
                id: "55c28229f78e2d64819b2a75"
            },
            state: state || "draft",
            configurations: []
        }, cb);
    }

    function updateProduct(id, payload, cb) {
        authenticatedRequest.put(
            {
                uri: "/products/" + id,
                body: payload
            }, function (err, response, body) {
                cb(err, body);
            });
    }


    describe('# POST /products', function () {

        it('should create a product with valid payload', function (done) {
            authenticatedRequest.post({
                uri: '/products',
                body: {
                    name: "valid product",
                    appearance: {
                        id: "1"
                    },
                    productType: {
                        id: "55c28229f78e2d64819b2a75"
                    },
                    configurations: [],
                    tags: []
                }
            }, function (err, response, body) {

                expect(response.statusCode).to.equal(201);
                expect(response.headers.location).to.exist;
                expect(response.headers.location).to.contain("/products/" + body.id);

                expect(body).to.exist;
                expect(body.creator.id).to.exist;
                expect(body.state).to.equal("draft");
                expect(body.productType.id).to.equal("55c28229f78e2d64819b2a75");

                done();
            });
        });

        it('should NOT create product with invalid payload', function (done) {

            authenticatedRequest.post('/products', {
                body: {
                    appearance: "12",
                    configurations: "12"
                }
            }, function (err, response, body) {

                expect(response.statusCode).to.equal(400);

                done();
            });

        });

        it('should NOT create product with invalid productType', function (done) {

            authenticatedRequest.post('/products', {
                body: {
                    appearance: {
                        id: "1"
                    },
                    productType: {
                        id: "123"
                    },
                    configurations: []
                }
            }, function (err, response, body) {

                expect(response.statusCode).to.equal(400);

                done();
            });

        });

        it('should NOT create product with invalid appearance', function (done) {

            authenticatedRequest.post('/products', {
                body: {
                    appearance: {
                        id: "99"
                    },
                    productType: {
                        id: "55c28229f78e2d64819b2a75"
                    },
                    configurations: []
                }
            }, function (err, response, body) {
                expect(response.statusCode).to.equal(400);

                done();
            });

        });

        it('should NOT create product without authentication', function (done) {

            baseRequest.post('/products', {}, function (err, response) {
                expect(response.statusCode).to.equal(403);

                done();
            });

        });

    });

    describe('# PUT /products/{id}', function () {

        it('should update product', function (done) {
            flow()
                .seq("product", function (cb) {
                    createProduct({
                        appearance: {
                            id: "1"
                        },
                        productType: {
                            id: "55c28229f78e2d64819b2a75"
                        },
                        configurations: []
                    }, cb);
                })
                .seq("updated", function (cb) {
                    authenticatedRequest.put("/products/" + this.vars.product.id,
                        {
                            body: {
                                appearance: {
                                    id: "2"
                                },
                                productType: {
                                    id: "55c28229f78e2d64819b2a75"
                                },
                                configurations: []
                            }
                        }, function (err, response, body) {
                            expect(response.statusCode).to.equal(200);
                            expect(body.appearance.id).to.equal("2");

                            cb(err, body);
                        });
                })
                .exec(done)
        });

        it('should NOT update other products', function (done) {
            flow()
                .seq("session", function (cb) {
                    baseRequest.post("/sessions", {
                        body: {
                            email: "newuser@hiptees.de",
                            auth: {
                                type: "test"
                            }
                        }
                    }, function (err, response, body) {
                        cb(err, body);
                    })
                })
                .seq("product", function (cb) {

                    baseRequest.post("/products", {
                        headers: {
                            "session-token": this.vars.session.id
                        },
                        body: {
                            appearance: {
                                id: "2"
                            },
                            productType: {
                                id: "55c28229f78e2d64819b2a75"
                            },
                            configurations: []
                        }
                    }, function (err, response, body) {
                        cb(err, body);
                    });

                })
                .seq("response", function (cb) {
                    authenticatedRequest.put('/products/' + this.vars.product.id, {
                        body: {
                            appearance: {
                                id: "2"
                            },
                            productType: {
                                id: "55c28229f78e2d64819b2a75"
                            },
                            configurations: []
                        }
                    }, cb)
                })
                .exec(function (err, results) {
                    expect(err).to.not.exist;
                    expect(results.response.statusCode).to.equal(403);

                    done(err);
                });

        });

        it('should NOT be possible without valid session', function (done) {
            flow()
                .seq("product", function (cb) {
                    createProduct({
                        appearance: {
                            id: "1"
                        },
                        productType: {
                            id: "55c28229f78e2d64819b2a75"
                        },
                        configurations: []
                    }, cb);
                })
                .seq("updated", function (cb) {
                    baseRequest.put("/products/" + this.vars.product.id,
                        {
                            body: {
                                appearance: {
                                    id: "2"
                                },
                                productType: {
                                    id: "55c28229f78e2d64819b2a75"
                                },
                                configurations: []
                            }
                        }, function (err, response, body) {
                            expect(response.statusCode).to.equal(403);

                            cb(err, body);
                        });
                })
                .exec(done)
        });

        it('should NOT update product if state unequal to "draft" ', function (done) {

            flow()
                .seq("product", function (cb) {
                    createEmptyProduct("private", cb);
                })
                .seq("response", function (cb) {
                    authenticatedRequest.put('/products/' + this.vars.product.id, {
                        body: {
                            appearance: {
                                id: "2"
                            },
                            productType: {
                                id: "55c28229f78e2d64819b2a75"
                            },
                            state: "draft",
                            configurations: []
                        }
                    }, cb);
                })
                .exec(function (err, results) {

                    expect(err).not.to.exist;
                    expect(results.response.statusCode).to.equal(400);

                    done();
                });

        });

    });


    describe('# PATCH /products/{id}', function () {

        var checkStateUpdate = function (from, to, expectStatus, done) {
            flow()
                .seq("product", function (cb) {
                    createEmptyProduct(from, cb);
                })
                .seq("response", function (cb) {
                    authenticatedRequest.patch('/products/' + this.vars.product.id, {
                        body: {
                            state: to
                        }
                    }, cb);
                })
                .exec(function (err, results) {
                    expect(err).not.to.exist;
                    expect(results.response.statusCode).to.equal(expectStatus);

                    done();
                });
        };

        it('should update state from draft to draft', function (done) {
            checkStateUpdate("draft", "draft", 200, done);
        });

        it('should update state from draft to public', function (done) {
            checkStateUpdate("draft", "public", 200, done);
        });

        it('should update state from draft to private', function (done) {
            checkStateUpdate("draft", "private", 200, done);
        });

        it('should update state from public to private', function (done) {
            checkStateUpdate("public", "private", 200, done);
        });

        it('should update state from private to public', function (done) {
            checkStateUpdate("private", "public", 200, done);
        });

        it('should NOT update state from private to draft', function (done) {
            checkStateUpdate("private", "draft", 400, done);
        });

        it('should NOT update state from public to draft', function (done) {
            checkStateUpdate("public", "draft", 400, done);
        });

        it('should NOT update with unknown state', function(done){
            checkStateUpdate("public", "unknown", 400, done);
        })

    });


})
;