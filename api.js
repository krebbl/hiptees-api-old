// setup http + express + socket.io
var Hapi = require('hapi'),
    Routes = require('./routes'),
    Inert = require('inert'),
    fs = require("fs"),
    flow = require('flow.js').flow,
    _ = require("lodash"),
    SessionScheme = require('./schemes/SessionScheme'),
    ImageRenderer = require('./util/ImageRenderer'),
    SprdConverter = require('./util/SprdConverter'),
    Mongoose = require('mongoose'),
    Images = require('./resources/images');

module.exports = {
    server: null,
    connection: null,
    start: function (ENV, callback) {
        var config = require('./env/' + ENV);

        Mongoose.connect('mongodb://' + config.database.host + '/' + config.database.db);
        var connection = this.connection = Mongoose.connection;
        connection.on('error', console.error.bind(console, 'connection error'));
        connection.once('open', function callback() {
            console.log("Connection with database succeeded.");
        });

        var server = this.server = new Hapi.Server();

        server.settings.app = {
            config: config
        };
        server.register(Inert, function () {
        });

        server.path("/api/v1");

        // configure Images resource
        //Images.config(config.server.uri);

        server.connection({port: config.server.port, host: config.server.host, uri: config.server.uri});

        server.auth.scheme("session", SessionScheme);
        server.auth.strategy("session-required", "session", true);

        var v1 = require('./v1');
        server.register({
            register: v1
        }, {
            routes: {
                prefix: "/api/v1"
            }
        }, function (err) {
            console.log(err);
        });

        server.route({
            method: "GET",

            path: "/renderer/{param*}",
            config: {
                auth: false,
                handler: {
                    directory: {
                        path: config.appPath,
                        listing: false
                    }
                }
            }
        });

        server.route({
            method: "GET",
            path: "/api/v1/images/productTypes/{param*}",

            config: {
                auth: false,
                files: {
                    relativeTo: "./"
                },
                handler: {
                    directory: {
                        path: "images",
                        listing: false
                    }
                }
            }
        });


        ImageRenderer.init({
            baseUrl: "http://" + config.server.host + ":" + config.server.port,
            targetDir: process.cwd() + "/generated/images"
        }, function (err) {
            console.log(err);
            console.log("image renderer initialized");
        });

        SprdConverter.init(config.spreadshirt);

        server.start(function (err) {
            console.log('Server started ', server.info.uri);

            callback && callback(err, server, config, connection);
        });

    },

    loadFixtures: function (done) {
        var fixtures = require('./fixtures/fixtures'),
            connection = this.connection;

        flow()
            .parEach(fixtures, function (values, cb) {
                var collection = connection.collections[this.key];

                flow()
                    .seq(function () {
                        collection.remove();
                    })
                    .seq(function (cb) {
                        if (values.length) {
                            // load fixtures
                            collection.insert(values, cb);
                        } else {
                            cb();
                        }
                    })
                    .exec(cb);

            })
            .exec(done);

    },

    stop: function () {
        if (this.server) {
            this.server.stop();
        }
    }
};