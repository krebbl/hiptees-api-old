var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema,
    timestamps = require('mongoose-timestamp');

var STATE = {
    DRAFT: "draft",
    PRIVATE: "private",
    PUBLIC: "public",
    DELETED: "deleted"
};

var STATE_VALUES = [STATE.DRAFT, STATE.PRIVATE, STATE.PUBLIC, STATE.DELETED];

var ALLOWED_STATE_CHANGES = {
    "draft": STATE_VALUES,
    "private": [STATE.PRIVATE, STATE.PUBLIC, STATE.DELETED],
    "public": [STATE.PRIVATE, STATE.PUBLIC, STATE.DELETED],
    "delete": []
};

var Product = new Schema({
    name: {
        type: String,
        required: false
    },
    resourceProvider: {
        type: String,
        required: false
    },
    "productType": {
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'ProductType',
        required: true
    },
    "appearance.id": {
        type: String,
        required: true
    },
    configurations: {
        type: Array,
        required: false
    },
    "creator": {
        type: Mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: false
    },
    state: {
        type: String,
        enum: {
            values: STATE_VALUES,
            message: 'enum validator failed for path `{PATH}` with value `{VALUE}`'
        },
        required: true
    },
    tags: {
        type: Array,
        required: false
    },
    sprd: {
        type: Object,
        required: false
    }
});

Product.plugin(timestamps);

Product.statics.getAllowedStates = function (state) {
    if (state == STATE.DRAFT) {
        return STATE_VALUES;
    } else if (state == STATE.PRIVATE || state == STATE.PUBLIC) {
        return [STATE.PRIVATE, STATE.PUBLIC];
    } else {
        return STATE_VALUES;
    }
};

Product.methods.isStateAllowed = function (state) {
    return ALLOWED_STATE_CHANGES[this.state].indexOf(state) > -1;
};

Product.statics.STATE = STATE;


Product.set('toJSON', {getters: false, virtuals: true});


module.exports = Mongoose.model('Product', Product, 'products');