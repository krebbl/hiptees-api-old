var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema;

var Appearance = new Schema({
    id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    color: {
        type: String,
        required: true
    }
});

var Size = new Schema({
    id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    }
});

var ProductType = new Schema({
    name: {
        type: String,
        required: true
    },
    resourceProvider: {
        type: String,
        required: true
    },
    printArea: {
        type: Object,
        required: true
    },
    appearances: {
        type: [Appearance],
        required: true
    },
    size: {
        type: Object,
        required: true
    },
    mapping: {
        type: Object,
        required: true
    },
    sprdId: {
        type: String,
        required: true
    },
    sizes: {
        type: [Size],
        required: true
    }
});

ProductType.set('toJSON', {getters: false, virtuals: false});


module.exports = Mongoose.model('ProductType', ProductType, 'productTypes');