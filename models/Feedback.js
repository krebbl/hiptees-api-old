var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema,
    timestamps = require('mongoose-timestamp');


var Feedback = new Schema({
    text: {
        type: String,
        required: false
    },
    author: {
        type: Mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: false
    }
});


Feedback.plugin(timestamps);


Feedback.set('toJSON', {getters: false, virtuals: true});


module.exports = Mongoose.model('Feedback', Feedback, 'feedbacks');