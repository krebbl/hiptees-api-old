var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema;

var DesignSchema = new Schema({
    name: {
        type: String,
        required: false
    },
    size: {
        type: Object,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    "creator": {
        type: Mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: false
    }
});

DesignSchema.set('toJSON', { virtuals: true });


module.exports = Mongoose.model('Design', DesignSchema);