var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema;

var SessionSchema = new Schema({
    "user": {
        type: Mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    auth: {
        type: Object,
        required: false
    }
});

SessionSchema.set('toJSON', { virtuals: true });


module.exports = Mongoose.model('Session', SessionSchema);