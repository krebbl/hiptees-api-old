var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema;

var STATE = {
    NEW: "new",
    REGISTERED: "registered",
    DEACTIVATED: "deactivated"
};

var STATE_VALUES = [STATE.NEW, STATE.REGISTERED, STATE.DEACTIVATED];

var UserSchema = new Schema({
    name: {
        type: String,
        required: false
    },
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: false
    },
    roles: {
        type: Array,
        required: true
    },
    registrationDate: {
        type: Date,
        required: false
    },
    state: {
        type: String,
        enum: {
            values: STATE_VALUES,
            message: 'enum validator failed for path `{PATH}` with value `{VALUE}`'
        },
        required: true
    },
    profileImage: {
        type: String,
        required: false
    },
    // TODO: add fields, registration date
    oauth: {
        type: Object,
        required: false
    }
});

UserSchema.set('toJSON', { virtuals: true });

UserSchema.statics.STATE = STATE;


module.exports = Mongoose.model('User', UserSchema);