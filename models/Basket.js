var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema,
    timestamps = require('mongoose-timestamp');

var BasketItemSchema = new Schema({
    quantity: {
        type: Number,
        required: true
    },
    product: {
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'Product',
        required: true
    },
    "size.id": {
        type: String,
        required: true
    }

});

var BasketSchema = new Schema({
    items: {
        type: [BasketItemSchema],
        required: false
    }
});

BasketSchema.plugin(timestamps);


BasketSchema.set('toJSON', {virtuals: true});


module.exports = Mongoose.model('Basket', BasketSchema);