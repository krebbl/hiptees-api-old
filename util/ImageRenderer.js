var phantom = require('phantom'),
    flow = require('flow.js').flow,
    fs = require('fs'),
    im = require('imagemagick');

var Renderer = function (config) {
    this.ctor(config);
};

Renderer.prototype = {
    config: {
        baseUrl: "",
        viewportWidth: 1600,
        viewportHeight: 1600,
        sizes: [],
        maxPhantoms: 1,
        fragment: "/product/",
        imagePathFnc: function (id, size) {
            return id + "-" + size + ".png";
        }
    },

    ctor: function (config) {
        this.config = config;
        this.openPages = [];
        this.waitingQueue = [];
        this.renderingProcesses = [];
    },

    init: function (cb) {
        var config = this.config;
        var iterator = new Array(this.config.maxPhantoms);

        if (!config.baseUrl) {
            throw "No base url for render page defined";
        }

        var self = this;

        this.queueInterval = setInterval(function () {
            if (self.waitingQueue.length > 0 && self.openPages.length > 0) {
                var cb = self.waitingQueue.shift();
                cb(null, self.openPages.pop());
            }
        }, 50);

        flow()
            .seqEach(iterator, function (i, cb) {
                self._initPhantomPage(function (err, page) {
                    if (!err) {
                        self.openPages.push(page);
                    }
                    cb(err);
                })
            })
            .exec(cb);
    },

    _initPhantomPage: function (cb) {
        var viewportWidth = this.config.viewportWidth;
        var viewportHeight = this.config.viewportHeight;
        var baseUrl = this.config.baseUrl;

        phantom.create(function (ph) {
            ph.createPage(function (page) {
                page.setViewportSize(viewportWidth, viewportHeight, function () {
                    page.open(baseUrl + "/renderer/renderer.html", function (status) {
                        if (status !== "success") {
                            exited = true;
                            ph.exit();
                            cb && cb("renderer not reachable");
                        }
                    });
                    var exited = false;

                    page.set('onConsoleMessage', function (msg, lineNum, sourceId) {
                        console.log('CONSOLE: ' + msg + ' (from line #' + lineNum + ' in "' + sourceId + '")');
                        if (msg == "rendererStarted") {
                            cb(null, page);
                        }
                    });
                });
            });
        });
    },

    render: function (id, force, cb) {
        console.log("RENDERER: start rendering");
        if (force instanceof Function) {
            cb = force;
            force = false;
        }

        if (force) {
            var self = this;
            console.log("RENDERER: remove files");
            this.config.sizes.forEach(function (size) {
                try {
                    fs.unlinkSync(self.config.imagePathFnc(id, size));
                } catch (e) {
                    console.log(e);
                }
            });
        }

        var p = this._getRenderingProcess(id);
        console.log("RENDERER: find process");
        if (p && force) {
            p.cancel();
            p = null;
        }
        if (!p) {
            console.log("RENDERER: no process found");
            p = this._createRenderingProcess(id, this);
            p.addCallback(cb);
            console.log("RENDERER: start process");
            p.run();
        } else {
            console.log("RENDERER: process found");
            p.addCallback(cb);
        }

    },


    _getRenderingProcess: function (id) {
        return this.renderingProcesses[id];
    },
    _createRenderingProcess: function (id) {
        var renderingProcesses = this.renderingProcesses;
        var p = {
            flow: null,
            callbacks: [],
            cancelled: false,
            cancel: function () {
                renderingProcesses[id] = null;
                this.cancelled = true;
            },
            addCallback: function (cb) {
                if (cb) {
                    this.callbacks.push(cb);
                }
            },
            run: function () {
                var cancelled = p.cancelled,
                    self = this;

                this.flow.exec(function (err) {
                    renderingProcesses[id] = null;
                    if (!cancelled) {
                        var cb;
                        while (self.callbacks.length) {
                            cb = self.callbacks.pop();
                            cb(err);
                        }

                    } else {
                        console.log("CANCELLED");
                    }
                });
            }

        };

        var self = this;

        p.flow = flow()
            .seq("base64", function (cb) {
                if (!p.cancelled) {
                    self._renderBase64(id, self.config.baseUrl, cb);
                } else {
                    this.end();
                }
            })
            .seq(function (cb) {
                if (!p.cancelled) {
                    self._writeImages(this.vars.base64, id, self.config.imagePathFnc, cb);
                } else {
                    this.end();
                }
            });

        this.renderingProcesses[id] = p;

        return p;
    },
    _renderBase64: function (id, baseUrl, cb) {
        var self = this;
        this._getPage(function (err, page) {
            var finished = false;
            var onFinished = function () {
                if (!finished) {
                    finished = true;
                    page.set('onConsoleMessage', null);
                    self.openPages.push(page);
                }
            };

            var emptyFnc = function () {
            };

            var time = new Date().getTime();
            page.set('onConsoleMessage', function (msg) {
                if (msg == "rendered " + id) {
                    console.log("message " + ((new Date().getTime()) - time));
                    page.renderBase64('PNG', function (base64) {
                        console.log("rendered " + ((new Date().getTime()) - time));
                        page.evaluate(function () {
                            window.location.hash = "";
                        }, emptyFnc);
                        onFinished();
                        cb && cb(null, base64);
                    });
                } else {
                    console.log(msg);
                }
            });


            page.evaluate(function (hash) {
                window.location.hash = hash;
            }, emptyFnc, self.config.fragment + id);

            setTimeout(function () {
                if (!finished) {
                    onFinished();
                    cb && cb("render timeout");
                }
            }, 30000);
        });
    },

    _writeImages: function (base64, id, pathFnc, cb) {
        flow()
            .seq("buf", function () {
                return new Buffer(base64, 'base64');
            })
            .parEach(this.config.sizes, function (size, cb) {
                im.resize({
                    srcData: this.vars.buf,
                    dstPath: pathFnc(id, size),
                    height: size
                }, function (err, stdIn, stdOut) {
                    cb(err);
                });
            })
            .exec(cb);
    },

    _getPage: function (cb) {
        if (this.openPages.length > 0) {
            cb(null, this.openPages.pop());
        } else {
            this.waitingQueue.push(cb);
        }
    }
};

var EMPTY_CB = function(){};

module.exports = {
    sizes: [300, 600, 900],
    config: null,
    productRenderer: null,
    init: function (config, done) {
        var self = this;
        this.config = config;

        this.productRenderer = new Renderer({
            viewportWidth: 1600,
            viewportHeight: 1600,
            sizes: this.sizes,
            fragment: "/product/",
            imagePathFnc: function (id, size) {
                return self.productImagePath(id, size);
            },
            maxPhantoms: 2,
            baseUrl: config.baseUrl
        });

        this.printoutRenderer = new Renderer({
            viewportWidth: "auto",
            viewportHeight: 4000,
            sizes: [4000],
            fragment: "/printout/",
            imagePathFnc: function (id, size) {
                return self.printoutImagePath(id);
            },
            maxPhantoms: 2,
            baseUrl: config.baseUrl
        });

        flow()
            .seq(function (cb) {
                self.productRenderer.init(cb);
            })
            .seq(function (cb) {
                self.printoutRenderer.init(cb);
            })
            .exec(done);
    },
    renderProduct: function (id, force, cb) {
        this.productRenderer.render(id, force, cb);
    },

    printoutImagePath: function (id) {
        return this.config.targetDir + "/printouts/" + id + ".png";
    },

    productImagePath: function (id, size) {
        return this.config.targetDir + "/products/" + id + "-" + size + ".png";
    },

    renderPrintout: function (id, force, cb) {
        this.printoutRenderer.render(id, force, cb);
    },
    checkAndRenderPrintout: function (id, cb) {
        var filePath = this.printoutImagePath(id),
            self = this;

        cb = cb || EMPTY_CB;

        fs.exists(filePath, function (exists) {
            if (exists) {
                cb(null, filePath);
            } else {
                self.renderPrintout(id, function (err) {
                    if (err) {
                        cb(err);
                    } else {
                        cb(null, filePath);
                    }
                });
            }
        });
    }
};