var flow = require('flow.js').flow,
    Product = require('../models/Product'),
    Basket = require('../models/Basket'),
    ProductType = require('../models/ProductType'),
    fs = require("fs"),
    ImageRenderer = require("./ImageRenderer"),
    request = require("request"),
    crypto = require('crypto'),
    _ = require('lodash');

var apiRequest = request.defaults({
    gzip: true,
    json: true,
    headers: {
        'content-type': 'application/json; charset=UTF-8'
    }
});

function SHA1(key) {
    var shasum = crypto.createHash('sha1');
    shasum.update(key);
    return shasum.digest('base64');
}

function signature(method, url, secret, time) {
    var data = method + " " + url + " " + time + " " + secret;
    return SHA1(data);
}

function createQueryParameter(method, url, apiKey, secret, sessionId) {
    var time = new Date().getTime();

    return {
        mediaType: "json",
        sig: signature(method, url, secret, time),
        time: time,
        method: method,
        apiKey: apiKey,
        sessionId: sessionId
    };
}

var sessionId;

module.exports = {
    config: null,
    init: function (config) {
        this.config = config;
    },

    createSprdDesign: function (printout, callback) {
        var config = this.config;
        flow()
            // create design
            .seq("design", function (cb) {
                var url = config.apiEndpoint + "/shops/" + config.shop + "/designs";

                console.log(url);
                apiRequest.post(url, {
                    qs: createQueryParameter("post", url, config.apiKey, config.secret),
                    body: {
                        description: "",
                        designServiceState: null,
                        name: printout,
                        restrictions: {}
                    }
                }, function (err, response, body) {
                    if (response && response.statusCode == 201) {
                        cb(null, body);
                    } else {
                        console.log(response);
                        cb("design failed : " + response.statusMessage);
                    }
                });
            })
            // upload shit
            .seq(function (cb) {
                console.log(this.vars.design.id);
                var stat = fs.statSync(printout);
                console.log(stat.size);
                fs.createReadStream(printout).pipe(request.put(config.imageEndpoint + "/designs/" + this.vars.design.id, {
                    qs: {
                        method: "put",
                        mediaType: "png",
                        apiKey: config.apiKey
                    },
                    headers: {
                        'Content-Length': stat.size
                    }
                }, function (err, response, body) {
                    if (response && response.statusCode == 200) {
                        console.log("uploaded image");
                        cb(null);
                    } else {
                        console.log(body);
                        cb("image upload failed : " + response.statusMessage);
                    }
                }))
            })
            .exec(function (err, results) {
                callback && callback(err, results.design);
            });

    },

    convertProductToArticle: function (productId, region, sessionId, cb) {
        var config = this.config;
        var self = this;
        flow()
            .seq("product", function (cb) {
                Product.findOne({_id: productId}).populate("productType").lean().exec(cb);
            })
            .seq("sprdProductType", function (cb) {
                apiRequest.get(config.apiEndpoint + "/shops/" + config.shop + "/productTypes/" + this.vars.product.productType.sprdId, {
                    qs: {
                        mediaType: "json"
                    }
                }, function (err, response, body) {
                    if (response && response.statusCode == 200) {
                        cb(err, body);
                    } else {
                        cb(err || "PRODUCT_TYPE_NOT_FOUND");
                    }
                });
            })
            .seq("printout", function (cb) {
                ImageRenderer.checkAndRenderPrintout(productId, cb);
            })
            .seq("sprdDesign", function (cb) {
                var product = this.vars.product;

                if (product.sprd && product.sprd.designId) {
                    apiRequest.get(config.apiEndpoint + "/shops/" + config.shop + "/designs/" + product.sprd.designId, {
                        qs: {
                            mediaType: "json"
                        }
                    }, function (err, response, body) {
                        if (response && response.statusCode == 200) {
                            cb(err, body);
                        } else {
                            cb(null);
                        }
                    });
                } else {
                    cb();
                }
            })
            .seq("sprdDesign", function (cb) {
                if (!this.vars.sprdDesign) {
                    self.createSprdDesign(this.vars.printout, cb);
                } else {
                    cb(null, this.vars.sprdDesign);
                }
            })
            .seq(function (cb) {
                var product = this.vars.product;
                if (!(product.sprd && product.sprd.designId)) {
                    Product.update({_id: productId}, {
                        $set: {
                            sprd: {
                                designId: this.vars.sprdDesign.id
                            }
                        }
                    }, cb);
                } else {
                    cb();
                }
            })
            .seq("sprdConfig", function () {
                return {
                    offset: {
                        x: 0,
                        y: 30  // TODO: correct offset
                    },
                    content: {
                        dpi: "25.4",
                        svg: {
                            image: {
                                designId: this.vars.sprdDesign.id,
                                height: 250,
                                printColorRGBs: "",
                                transform: "",
                                width: 308 // TODO: printAreaWidth
                            }
                        }
                    },
                    printArea: {
                        id: 4 // TODO: printArea id
                    },
                    printType: {
                        id: 17
                    },
                    type: "design"
                };

            })
            .seq("sprdProduct", function () {
                return {
                    appearance: {
                        id: this.vars.product.appearance.id
                    },
                    defaultValues: {
                        // TODO
                        defaultView: {
                            id: 1
                        }
                    },
                    productType: {
                        id: this.vars.sprdProductType.id
                    },
                    configurations: [this.vars.sprdConfig],
                    restrictions: {
                        customizable: true,
                        example: false,
                        freeColorSelection: true,
                        softBoundarySupported: true
                    }
                }
            })
            .seq("sprdProduct", function (cb) {
                var url = config.apiEndpoint + "/shops/" + config.shop + "/products";
                apiRequest.post(url, {
                    qs: createQueryParameter("post", url, config.apiKey, config.secret),
                    body: this.vars.sprdProduct
                }, function (err, response, body) {
                    if (response && response.statusCode == 201) {
                        cb(null, body);
                    } else {
                        console.log(body);
                        cb("product failed : " + response.statusMessage);
                    }
                });
            })
            .seq("sprdArticle", function (cb) {
                var url = config.apiEndpoint + "/shops/" + config.shop + "/articles";
                apiRequest.post(url, {
                    qs: createQueryParameter("post", url, config.apiKey, config.secret, sessionId),
                    body: {
                        "product": {
                            "id": this.vars.sprdProduct.id
                        },
                        "name": "", // optional for partner shops, mandatory on the marketplace
                        "description": "hiptees shirt", // optional
                        "tags": ["hiptees", productId].join(" "), // optional
                        "commission": {"vatIncluded": 5.09, "currency": {"id": "1"}} //optional
                    }
                }, function (err, response, body) {
                    if (response.statusCode == 201) {
                        cb(null, body);
                    } else {
                        cb("article creation failed failed : " + body.message);
                    }
                });
            })
            .exec(function (err, results) {
                if (err) {
                    console.log(err);
                    cb(err);
                } else {
                    cb(err, {
                        sprdProduct: results.sprdProduct,
                        sprdArticle: results.sprdArticle,
                        product: results.product
                    });
                }

            });
    },

    getSession: function (cb) {
        var config = this.config;
        var url = config.apiEndpoint + "/sessions";

        flow()
            .seq("session", function (cb) {
                var self = this;
                if (sessionId) {
                    apiRequest.get(url + "/" + sessionId, {
                        qs: createQueryParameter("post", url, config.apiKey, config.secret)
                    }, function (err, response, body) {
                        if (response && response.statusCode == 200) {
                            self.end(null, body);
                        } else {
                            cb(null)
                        }

                    })
                } else {
                    cb(null);
                }
            })
            .seq("session", function (cb) {
                apiRequest.post(url, {
                    qs: createQueryParameter("post", url, config.apiKey, config.secret),
                    body: {
                        username: config.username,
                        password: config.password
                    }
                }, function (err, response, body) {
                    if (response && response.statusCode !== 201) {
                        cb("login failed");
                    } else {
                        if (!err) {
                            sessionId = body.id;
                        }
                        cb(err, body);
                    }
                })
            })
            .exec(function (err, results) {
                cb(err, results.session);
            });

    },

    convertBasket: function (basketId, cb) {
        var self = this,
            productSprdMap = {},
            config = this.config;
        flow()
            .seq("basket", function (cb) {
                // load basket
                Basket.findOne({
                    _id: basketId
                }).populate("items.product").exec(function (err, basket) {
                    if (!err && !basket) {
                        err = Boom.notFound("basket not found");
                    }
                    cb(err, basket);
                });
            })
            .seq("session", function (cb) {
                self.getSession(cb);
            })
            .seq(function (cb) {
                var productIds = [],
                    sprdProducts = [],
                    platform = "EU" || "NA"; // todo implement

                this.vars.basket.items.forEach(function (item) {
                    productIds.push(item.product.id.toString());
                });

                var session = this.vars.session;

                productIds = _.unique(productIds);

                flow()
                    .seqEach(productIds, function (productId, cb) {
                        self.convertProductToArticle(productId, null, session.id, function (err, result) {
                            if (!err) {
                                productSprdMap[productId] = {
                                    product: result.sprdProduct,
                                    productType: result.product.productType.sprdId,
                                    article: result.sprdArticle
                                };
                            }
                            cb(err);
                        })
                    })
                    .exec(function (err) {
                        cb(err);
                    });
            })
            .seq("sprdBasket", function (cb) {
                // TODO: implement
                var basketItems = [];

                this.vars.basket.items.forEach(function (item) {

                    var sprdProduct = productSprdMap[item.product.id.toString()];
                    basketItems.push({
                        quantity: item.quantity,
                        element: {
                            href: sprdProduct.product.href,
                            id: sprdProduct.product.id,
                            type: "sprd:product",
                            properties: [
                                {
                                    key: "article",
                                    value: sprdProduct.article.id
                                },
                                {
                                    key: "size",
                                    value: item.size.id
                                },
                                {
                                    key: "appearance",
                                    value: item.product.appearance.id
                                },
                                {
                                    key: "productType",
                                    value: sprdProduct.productType
                                }
                            ]
                        },
                        shop: {
                            id: config.shop
                        },
                        origin: {
                            id: "4"
                        }
                    });

                });

                var basket = {
                    "currency": {
                        "href": "http://api.spreadshirt.net/api/v1/currencies/1",
                        "id": "1"
                    },
                    "language": {
                        "href": "http://api.spreadshirt.net/api/v1/languages/1",
                        "id": "1"
                    },
                    "country": {
                        "href": "http://api.spreadshirt.net/api/v1/countries/1",
                        "id": "1"
                    },
                    basketItems: basketItems
                };

                var url = config.apiEndpoint + "/baskets/";
                apiRequest.post(url, {
                    qs: createQueryParameter("post", url, config.apiKey, config.secret),
                    body: basket
                }, function (err, response, body) {
                    if (response && response.statusCode == 201) {
                        cb(null, body);
                    } else {
                        cb("basket creation failed failed : " + body.message);
                    }
                });

            })
            .exec(function (err, results) {
                cb(err, results.sprdBasket);
            });
    }

};